// JQuery for the research question create form

var DEPT_GEISEL_WRAP = '#id_department_geisel-wrapper';
var DEPT_DHMC_WRAP = '#id_department_dhmc-wrapper';
var DEPT_ALL_WRAP = '#id_department_all-wrapper'
var DEPT_GEISEL_VALUE = '#id_department_geisel-deck span';
var DEPT_DHMC_VALUE = '#id_department_dhmc-deck span';
var DEPT_ALL_VALUE = '#id_department_all-deck span'
var DEPT_GEISEL_ELM = '#id_department_geisel-autocomplete'
var DEPT_DHMC_ELM = '#id_department_dhmc-autocomplete'
var DEPT_ALL_ELM = '#id_department_all-autocomplete'

var DEPT_OTHER = '#id_department_other'

var TOPIC_WRAP = '#id_topic-wrapper'
var TOPIC_VALUE = '#id_topic-deck span'
var TOPIC_ELM = '#id_topic-autocomplete'

// Show or hide dependent elements determined by selected text
function toggle_show_hide_fast(element_id, dependent_id, test_text) {
    var elm = $(element_id);
    var dependent_elm = $('#id_' + dependent_id);

    if (elm.text() !== test_text) {
        dependent_elm.hide();
    } else {
        // Explicitly show it if it is Other because
        // the default is set to hide.
        dependent_elm.show();
    }
}

function toggle_show_hide_slow(selected_text, dependent_elm, test_text) {
    if (selected_text.indexOf("Other") >= 0) {
        dependent_elm.show('slow');
    } else {
        dependent_elm.hide('slow');
    }
}

function toggle_department_lists(institution) {
    // Do this the long way to start and see if there is a prettier
    // way to code this.

    if (institution == 'Dartmouth Hitchcock Medical Center') {
        $(DEPT_GEISEL_WRAP).hide();
        $(DEPT_ALL_WRAP).hide();
        $(DEPT_DHMC_WRAP).show();
        // Toggle department other
        toggle_show_hide_slow(
            $(DEPT_DHMC_VALUE).text(),
            $(DEPT_OTHER),
            'Other');
    }
    else if (institution == 'Dartmouth College / Geisel') {
        $(DEPT_DHMC_WRAP).hide();
        $(DEPT_ALL_WRAP).hide();
        $(DEPT_GEISEL_WRAP).show();
        // Toggle department other
        toggle_show_hide_slow(
            $(DEPT_GEISEL_VALUE).text(),
            $(DEPT_OTHER),
            'Other');
    }
    else if (institution == 'Other' || institution == 'Please select') {
        $(DEPT_GEISEL_WRAP).hide();
        $(DEPT_DHMC_WRAP).hide();
        $(DEPT_ALL_WRAP).show();
        // Toggle department other
        toggle_show_hide_slow(
            $(DEPT_ALL_VALUE).text(),
            $(DEPT_OTHER),
            'Other');
    }
}

$(document).ready(function() {

    var topic = $(TOPIC_ELM);
    var topic_remove = $('#id_topic-deck span span');
    var topic_other = $('#id_topic_other');

    var institution = $('#institution');
    var institution_other = $('#id_institution_other');

    var department_dhmc = $(DEPT_DHMC_ELM);
    var department_geisel = $(DEPT_GEISEL_ELM);
    var department_all = $(DEPT_ALL_ELM);
    var department_other = $(DEPT_OTHER);

    var need_lab_data = $("input[name=need_lab_data]");
    var need_lab_data_specify = $('#id_need_lab_data_specify');

    toggle_show_hide_fast(TOPIC_VALUE, 'topic_other', 'Other');
    toggle_show_hide_fast('#institution', 'institution_other', 'Other');
    toggle_department_lists(institution.find(":selected").text())
    toggle_show_hide_fast('need_lab_data', 'need_lab_data_specify', '1');

    // $('#id_topic-deck').on('change', TOPIC_VALUE, function() {
    topic.blur(function() {
        toggle_show_hide_slow($(TOPIC_VALUE).text(), topic_other, 'Other');
    });

    institution.change(function() {
        var selected_institution = $(this).find(":selected").text();
        toggle_show_hide_slow(selected_institution, institution_other, 'Other');
        // Modify Deparment list according to selected institution
        toggle_department_lists(selected_institution);
    });

    department_dhmc.blur(function() {
        toggle_show_hide_slow($(DEPT_DHMC_VALUE).text(), department_other, 'Other');
    });
    department_geisel.blur(function() {
        toggle_show_hide_slow($(DEPT_GEISEL_VALUE).text(), department_other, 'Other');
    });
    department_all.blur(function() {
        toggle_show_hide_slow($(DEPT_ALL_VALUE).text(), department_other, 'Other');
    });

    need_lab_data.blur(function() {
        toggle_show_hide_slow($('input[name=need_lab_data]:checked').val(), need_lab_data_specify, '1');
    });
})
