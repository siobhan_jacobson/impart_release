# -*- coding: utf-8 -*-
"""
Routing for authentication.

.. moduleauthor:: bl
.. versionadded:: Acai_Berry
"""

from django.conf.urls import url
from django.contrib.auth import views as auth_views

from authentication import views, forms

app_name = 'auth'

urlpatterns = [
    url(
        r'^login/$',
        views.LoginView.as_view(
            form_class=forms.ImpartAuthenticationForm),
        name="login"
    ),
    url(
        r'^logout/$',
        auth_views.logout,
        name='logout'
    ),
    url(
        r'^get_net_id/$',
        views.GetNetIDAjaxView.as_view(),
        name="getNetID"
    ),
    url(r'^ratelimit/$',
        views.RemoveLimitView.as_view(
            form_class=forms.RemoveRateLimitForm),
        name='ratelimit'),
]
