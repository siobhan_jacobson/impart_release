# -*- coding: utf-8 -*-
"""
Tests for authentication.

.. moduleauthor:: bgl
.. moduleauthor:: sba
.. moduleauthor:: saj
.. versionadded:: Acai_Berry
"""

from unittest import skipIf
from mock import patch
from ldap import SERVER_DOWN

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.core.cache import cache
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse
from django.test import TestCase, Client, RequestFactory

from authentication.utilities import get_num_attempts
from authentication.views import LoginView

from .forms import RemoveRateLimitForm
from authentication.backends.DartmouthLDAPConnector import DartLDAP
from authentication.backends.DartmouthLDAPConnector import main as ldapmain
from authentication.backends.ImpartAuthBackend import DartBackend
from authentication.backends.ImpartAuthBackend import main as backendmain


class DartLDAPTestCase(TestCase):

    is_ldap_setup = hasattr(settings, 'LDAP_USER') and hasattr(
        settings, 'LDAP_PASSWORD')

    def setUp(self):

        self.client = Client()

        self.local_user_username = 'temporary'
        self.local_user_password = 'temporary'
        self.local_user_email = 'temporary@gmail.com'
        # To be removed when implementing mock
        self.mail = 'siobhan.a.jacobson@dartmouth.edu'
        self.sn = 'Jacobson'

        self.garbage = 'BAD_VALUE'

        # Create local Django user for which to test.

        self.authenticator = DartLDAP()
        self.garbage = 'BAD_VALUE'

        if self.is_ldap_setup:
            self.username = settings.LDAP_USER
            self.password = settings.LDAP_PASSWORD
        else:
            self.username = 'd32710w'

        User.objects.create_user(
            self.local_user_username,
            self.local_user_email,
            self.local_user_password
        )

        self.user = User.objects.get(username=self.local_user_username)

        self.user_info_keys = [
            'first_name',
            'last_name',
            'group',
            'uid',
            'source',
            'email_address']

        # Create Dartmouth LDAP user for which to test with.
        # get_user_model().objects.create_user()

    def test_repr(self):
        self.assertEquals(self.authenticator.__repr__(),
            'Server: ldaps://oud.dartmouth.edu, Filter: (uid=), User DN: None, User CN: None')

    def test_str(self):
        self.assertEquals(
            self.authenticator.__str__(), 'Dartmouth LDAP Authenication')
        self.assertEquals(type(self.authenticator.__str__()), str)

    def test_unicode(self):
        self.assertEquals(
            self.authenticator.__unicode__(), u'Dartmouth LDAP Authenication')
        self.assertEquals(type(self.authenticator.__unicode__()), unicode)

    def test_no_ldap(self):
        self.authenticator._Server_ = 'ldaps://nosuchserver.atthis.domain'
        self.assertRaises(
            SERVER_DOWN,
            self.authenticator.authenticate_user_and_get_attributes,
            self.garbage, self.garbage)

    def test_bad_search_filter(self):
        count, user = self.authenticator.get_user_attributes(
            self.garbage, '&(poop)')
        self.assertIsNone(count)
        self.assertIsNone(user)

    def test_no_password(self):
        auth_success, attributes = self.authenticator.authenticate_user_and_get_attributes(
            self.garbage, '')
        self.assertFalse(auth_success)
        self.assertIsNone(attributes)

    def test_bad_username(self):
        auth_success, attributes = self.authenticator.authenticate_user_and_get_attributes(
            self.garbage, self.garbage)
        self.assertFalse(auth_success)
        self.assertIsNone(attributes)

    def test_bad_password(self):
        auth_success, attributes = self.authenticator.authenticate_user_and_get_attributes(
            self.username, self.garbage)
        self.assertFalse(auth_success)
        self.assertIsNone(attributes)

    @skipIf(not is_ldap_setup, 'LDAP setting need to exist.')
    def test_good_auth(self):
        auth_success, attributes = self.authenticator.authenticate_user_and_get_attributes(
            self.username, self.password)
        self.assertTrue(auth_success)
        self.assertIsNotNone(attributes)

    def test_get_user_attributes_single(self):
        count, attributes = self.authenticator.get_user_attributes(self.mail)
        self.assertEquals(count, 1)

    @skipIf(not is_ldap_setup, 'LDAP setting need to exist.')
    def test_get_user_attributes_single_using_netid(self):
        count, attributes = self.authenticator.get_user_attributes(
            self.username, 'uid')
        self.assertEquals(count, 1)

    def test_get_user_attributes_multiple_using_sn(self):
        count, attributes = self.authenticator.get_user_attributes(
            self.sn, 'sn')
        self.assertGreater(count, 1)

    @skipIf(not is_ldap_setup, 'LDAP setting need to exist.')
    def test_main(self):

        with patch('__builtin__.raw_input', return_value=self.username):
            with patch('getpass.getpass', return_value=self.password):
                auth_success, user_info = ldapmain()
                self.assertTrue(auth_success)
                for key in self.user_info_keys:
                    self.assertIn(key, user_info)

    @skipIf(not is_ldap_setup, 'LDAP setting need to exist.')
    def test_authentication_successful(self):
        auth_success, attributes = self.authenticator.authenticate_user_and_get_attributes(
            self.username, self.password)
        self.assertTrue(auth_success)
        self.assertIsNotNone(attributes)


class AuthenticationMethodTestCase(TestCase):

    is_ldap_setup = hasattr(settings, 'LDAP_USER') and hasattr(
        settings, 'LDAP_PASSWORD')

    def setUp(self):

        self.client = Client()

        self.local_user_username = 'temporary'
        self.local_user_password = 'temporary'
        self.local_user_email = 'temporary@gmail.com'
        # To be removed when implementing mock
        self.mail = 'siobhan.a.jacobson@dartmouth.edu'
        self.sn = 'Jacobson'

        self.garbage = 'BAD_VALUE'
        self.ImpartAuthBackend_string = 'Inspire Authentication Module'

        # Create local Django user for which to test.

        self.authenticator = DartLDAP()
        self.django_authenticator = DartBackend()
        self.garbage = 'BAD_VALUE'

        if self.is_ldap_setup:
            self.username = settings.LDAP_USER
            self.password = settings.LDAP_PASSWORD
        else:
            self.username = 'd32710w'

        User.objects.create_user(
            self.local_user_username,
            self.local_user_email,
            self.local_user_password
        )

        self.user = User.objects.get(username=self.local_user_username)

        self.user_info_keys = [
            'first_name',
            'last_name',
            'group',
            'uid',
            'physical_address',
            'source',
            'department',
            'email_address'
        ]

        # Create Dartmouth LDAP user for which to test with.
        # get_user_model().objects.create_user()

    def test_repr(self):
        self.assertEquals(
            self.django_authenticator.__repr__(),
            self.ImpartAuthBackend_string)

    def test_str(self):
        self.assertEquals(
            self.django_authenticator.__str__(), self.ImpartAuthBackend_string)
        self.assertEquals(type(self.django_authenticator.__str__()), str)

    def test_unicode(self):
        self.assertEquals(
            self.django_authenticator.__unicode__(),
            unicode(self.ImpartAuthBackend_string))
        self.assertEquals(
            type(self.django_authenticator.__unicode__()), unicode)

    def test_local_user_login(self):
        bad_login = self.client.login(
            username=self.local_user_username, password=self.garbage)
        good_login = self.client.login(
            username=self.local_user_username,
            password=self.local_user_password)
        self.assertFalse(bad_login)
        self.assertTrue(good_login)
        self.client.logout()

    def test_get_user_exists(self):
        from django.contrib.auth import get_user_model
        self.assertEquals(self.django_authenticator.get_user(
            get_user_model().objects.order_by('id').first().id).get_username(),
            self.local_user_username)

    def test_get_user_does_not_exist(self):
        self.assertIsNone(self.django_authenticator.get_user(5))

    def test_authenticate_return_value(self):
        with self.assertRaises(PermissionDenied):
            self.django_authenticator.authenticate(
                username=self.local_user_username,
                password=self.local_user_password)

    @skipIf(not is_ldap_setup, 'LDAP setting need to exist.')
    def test_ldap_user_login(self):
        good_login = self.client.login(
            username=self.username, password=self.password)
        self.assertTrue(good_login)
        self.client.logout()

    def test_user_profile_no_permissions(self):
        self.assertFalse(self.user.has_perm('authentication.completed_survey'))

    def test_login_page_has_no_permissions(self):

        response = self.client.post(
            reverse(u'auth:login'),
            {'username': self.local_user_username,
             'password': self.local_user_password})
        self.assertRedirects(
            response, reverse(u'crowd_sourcing:index'),
            status_code=302, target_status_code=200, msg_prefix='')

    def test_complete_permissions_form(self):
        pass

    def test_login_page_has_permissions(self):

        # self.perm_helper(DartProfile, 'completed_survey')
        response = self.client.post(
            reverse(u'auth:login'),
            {'username': self.local_user_username,
             'password': self.local_user_password}, follow=True)

        self.assertRedirects(
            response, reverse(u'crowd_sourcing:index'),
            status_code=302, target_status_code=200, msg_prefix='')

    @skipIf(not is_ldap_setup, 'LDAP setting need to exist.')
    def test_main(self):

        with patch('__builtin__.raw_input', return_value=self.username):
            with patch('getpass.getpass', return_value=self.password):
                inspire_user = backendmain()
                self.assertIsInstance(inspire_user, User)

    def test_logout_page(self):
        pass


class LoginViewTestCase(TestCase):

    """
    Test the Login View.
    """

    def setUp(self):
        """
        Create some default objects for testing.
        """

        self.user = User.objects.create_user('eve', 'e@example.com', 'pwd')

    def test_get_context_data(self):
        """
        Test that the context variables are properly added with next.
        """

        response = self.client.get(
            '{0}?next=crowd_sourcing/research_question'.format(
                reverse('auth:login')))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertContains(
            response, 'Sign in to Impart')

    def test_get_context_data_no_extra_data(self):
        """
        Test that the context variables are not added without next.
        """

        response = self.client.get(reverse('auth:login'))
        self.assertEqual(response.status_code, 200)
        self.assertFalse('heading_to_request_link' in response.context[-1])

        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertContains(
            response, 'Sign in to Impart')


class RateLimitingTestCase(TestCase):

    """
    Test the functionality of the rate limiting the LoginView in
    authentication.views. Test case will build off a lot of what is in
    the views above.

    ..note:: According to Django 1.6 documentation:
    Caches are not cleared after each test,
    and running "manage.py test fooapp" can insert data from the tests
    into the cache of a live system if you run your tests
    in production because, unlike databases, a separate "test cache"
    is not used. This behavior may change in the future.
    """

    def setUp(self):
        """
        Create default objects for testing.
        """
        cache.clear()
        self.client = Client()
        self.local_user_username = 'temporary'
        self.local_user_password = 'temporary'
        self.local_user_email = 'temporary@gmail.com'
        User.objects.create_user(
            self.local_user_username,
            self.local_user_email,
            self.local_user_password
        )

    def test_multiple_dispatches(self):
        """
        Check to ensure that the number of login attempts does
        not exceed the number determined by the RateLimitMixIn.
        It currently uses the default value of ratelimit_rate in the mixin,
        because the LoginView is using the default ratelimit_rate.
        """
        for x in range(0, int(LoginView.ratelimit_rate[0])):
            response = self.client.post(
                '/accounts/login/', {'username': 'john', 'password': 'smith'})
            self.assertEqual(response.status_code, 200)
        response = self.client.post(
            '/accounts/login/', {'username': 'john', 'password': 'smith'})
        # The first post request after the ratelimit has been reached should
        # return a 403 response
        self.assertEqual(response.status_code, 403)

    def test_get_attempts_function(self):
        """
        On a new user's first login attempt,
        the cache should add a new 'rl' key that keeps track
        of the number of login attempts per unit of time.
        """
        req = RequestFactory().post(
            '/accounts/login/', {'username': self.local_user_username,
                                 'password': self.local_user_password})
        rl_key = 'rl:' + req.META.get('HTTP_X_FORWARDED_FOR',
                                      req.META['REMOTE_ADDR'])

        # These tests are passing even though they should fail.
        # This is an issue with using the cache while testing.
        self.assertEqual(get_num_attempts(rl_key), None)
        self.assertEqual(get_num_attempts('127.0.0.1'), None)


class RateLimitFormTestCase(TestCase):

    """
    Test that submission of a valid IP address returns a valid response,
    and that an invalid IP also returns a valid response.
    """
    def setUp(self):
        """
        Create default objects for testing.
        """
        self.local_user_username = 'temporary'
        self.local_user_password = 'temporary'

    def test_validation(self):
        """
        Check how well the form validation works.
        """

        # Should fail, no data
        form_data = {
        }
        form = RemoveRateLimitForm(data=form_data)
        self.assertFalse(form.is_valid())

        # Should pass, valid entry
        form_data = {
            'IP': '127.0.0.1'
        }
        form = RemoveRateLimitForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Add a entry to test the removal of the limited IP
        RequestFactory().post(
            '/accounts/login/', {'username': self.local_user_username,
                                 'password': self.local_user_password})
        form_data = {
            'IP': '127.0.0.1'
        }
        form = RemoveRateLimitForm(data=form_data)
        self.assertTrue(form.is_valid())
        form.remove_key()

        # Should fail, data is in wrong format
        form_data = {
            'IP': 'adfasdfasdfasdfsadf'
        }
        form = RemoveRateLimitForm(data=form_data)
        self.assertFalse(form.is_valid())

    def test_RemoveRateLimitForm_view_get(self):
        """
        Test that the correct page is requested
        after the form is submitted by a non-superuser account.
        """
        # should fail, return 403 if not superuser
        form_data = {
            'IP': '127.0.0.1'
        }
        response = self.client.post(
            reverse('auth:ratelimit'), form_data)
        self.assertEqual(response.status_code, 403)

    def test_RemoveRateLimitForm_view_get_as_superuser(self):
        """
        Test that the correct page is requested
        after the form is submitted by a superuser.
        """

        username = 'eve2'
        user_pwd = 'eve2_pwd'
        self.user = get_user_model().objects.create_user(
            username, 'eve@example.com', user_pwd)
        self.user.is_superuser = True
        self.user.save()
        self.user = get_user_model().objects.get(id=self.user.id)
        self.assertTrue(self.user.is_superuser)
        # self.perm_helper(DartProfile, 'completed_survey')

        self.assertTrue(self.client.login(
            username=username, password=user_pwd))
