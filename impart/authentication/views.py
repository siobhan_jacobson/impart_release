# -*- coding: utf-8 -*-
"""
Controller logic for authentication.

.. moduleauthor:: bgl
.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

import logging
# import random
# import ratelimit
from datetime import datetime

# from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views import generic
from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy

# from django.contrib.auth.models import User
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth import login

from braces.views import LoginRequiredMixin
from braces.views import JSONResponseMixin
from braces.views import SuperuserRequiredMixin
from ratelimit.mixins import RatelimitMixin

from authentication.backends.DartmouthLDAPConnector import DartLDAP
from authentication.forms import ImpartAuthenticationForm
from authentication.forms import RemoveRateLimitForm
from authentication.utilities import remove_ratelimit_key
from authentication.utilities import get_num_attempts


log = logging.getLogger(__name__)


class ImpartLoginRequired(LoginRequiredMixin):

    """Make sure user is authenticated and has filled out profile."""


class LoginView(RatelimitMixin, FormView):

    """
    This is a class based version of django.contrib.auth.views.login.
    Inherits from RatelimitMixin to implement rate limiting.

    Usage: in urls.py::

        url(r'^login/$',
        LoginView.as_view(form_class=MyCustomAuthFormClass),
        name="login"),

    """

    form_class = ImpartAuthenticationForm
    redirect_field_name = REDIRECT_FIELD_NAME
    template_name = 'registration/login.html'
    default_redirect_url = reverse_lazy(u'crowd_sourcing:index')

    ratelimit_key = False
    ratelimit_block = True
    ratelimit_key = lambda group, req: req.META.get(
        'X_FORWARDED_PROTOCOL',
        req.META['REMOTE_ADDR'])
    ratelimit_rate = '5/m'

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        """Wrap the dispatch method in some decorators."""
        return super(LoginView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        """
        Check and delete the test cookie and log in the user.

        The user has provided valid credentials (this was checked in
        AuthenticationForm.is_valid()). So now we can check the test
        cookie stuff and log him in.
        """
        self.incmg_addr = self.request.META.get(
            'HTTP_X_FORWARDED_FOR', self.request.META['REMOTE_ADDR'])
        self.check_and_delete_test_cookie()
        login(self.request, form.get_user())
        log.info('Client at ' + self.incmg_addr
                 + ' has attempted to log in ' +
                 str(get_num_attempts(self.incmg_addr))
                 + ' time(s) with a ratelimit of ' + self.ratelimit_rate
                 + '. Most recent attempt was at ' + str(datetime.now()))
        return super(LoginView, self).form_valid(form)

    def form_invalid(self, form):
        """
        Reset the test cookie and redisplay the form.

        The user has provided invalid credentials (this was checked in
        AuthenticationForm.is_valid()). So now we set the test cookie
        again and re-render the form with errors.
        """

        self.incmg_addr = self.request.META.get(
            'HTTP_X_FORWARDED_FOR', self.request.META['REMOTE_ADDR'])
        self.set_test_cookie()
        log.info('Client at ' + self.incmg_addr
                 + ' has attempted to log in ' +
                 str(get_num_attempts(self.incmg_addr))
                 + ' time(s) with a ratelimit of ' + self.ratelimit_rate
                 + '. Most recent attempt was at ' + str(datetime.now()))
        return super(LoginView, self).form_invalid(form)

    def get_success_url(self):
        """
        Decide which url to redirect to. Will also remove the ratelimit key
        for the user logging in from the cache.

        :returns: The appropriate url.
        :rtype: str
        """

        if self.success_url:
            redirect_to = self.success_url
        else:
            redirect_to = self.request.GET.get(
                self.redirect_field_name, '')

        if not redirect_to:
            redirect_to = self.default_redirect_url
        # Security check -- don't allow redirection to a different host.
        elif not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = self.default_redirect_url

        log.debug(
            'Successful login. Calling remove_ratelimit_key function now.')
        remove_ratelimit_key(self.incmg_addr)

        return redirect_to

    def set_test_cookie(self):
        """Set the test cookie in self.request.session."""
        self.request.session.set_test_cookie()

    def check_and_delete_test_cookie(self):
        """
        Make sure the test cookie worked, then delete it.

        :returns: Whether the test cookie worked.
        :rtype: bool
        """
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
            return True
        return False

    def get(self, request, *args, **kwargs):
        """Same as django.views.generic.edit.ProcessFormView.get, but adds test cookie stuff."""
        self.set_test_cookie()
        return super(LoginView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        Extend to inject a flag indicating heading to a help request form.

        :param kwargs: Optional set of keyword arguments to pass to super().
        :returns: With context variable added.
        :rtype: Context
        """
        context = super(LoginView, self).get_context_data(**kwargs)

        context['no_nav_bar'] = True
        context['app_name'] = 'crowd_sourcing'
        next_str = self.request.GET.get('next')
        if next_str:
            context['next'] = next_str
            if 'comm_crowd_sourcing' in next_str:
                context['app_name'] = 'comm_crowd_sourcing'

        return context


class GetNetIDAjaxView(JSONResponseMixin, generic.View):

    """Return a NetID for the supplied email address."""

    def get(self, request):
        """
        Get the user's NetID from their email address.

        :returns: JSON string with user's NetID and/or any error information.
        :rtype: HTTPReponse
        """
        LookUpUser = DartLDAP()
        email_address = request.GET.get("email_address")
        result = {}
        try:
            results_count, user_information = LookUpUser.get_user_attributes(
                email_address)
            if not results_count or not user_information[0].get('uid'):
                result = {
                    "error": "Sorry, there's no NetID listed for that address.",
                    "netID": ""
                }
            else:
                result = {"error": "", "netID": user_information[0].get('uid')}
        except Exception:
            result = {
                "error": "Couldn't connect to the DND server.", "netID": ""}

        return self.render_json_response(result)


class RemoveLimitView(LoginRequiredMixin, SuperuserRequiredMixin, FormView):

    """
    A view that displays a form for removing a rate-limit key from
    the cache. Redirects to the hompage after a valid form is submitted.
    """
    template_name = 'auth/remove_rl_key.html'
    form_class = RemoveRateLimitForm
    success_url = reverse_lazy(u'rate_requests:list')
    raise_exception = True

    def form_valid(self, form):
        """
        Upon a successful form submission, the form's remove_key function is
        called so that the ratelimit_key that corresponds
        to the IP address entered in the form is removed.

        :param form: The form containing the IP address entered.
        :type form: RemoveRateLimitForm
        """
        form.remove_key()
        return super(RemoveLimitView, self).form_valid(form)
