"""
Authentication and Profiles Utilities.

Utilities that are needed in Authenticationand Profiles
but may be useful in other libraries.
"""

from django.core.cache import cache
import logging
from datetime import datetime

log = logging.getLogger(__name__)


def remove_ratelimit_key(incmng_addr):
    """
    Removes the ratelimit key for a particular address from the cache,
    if it exists.

    :param incmng_addr: The address to be concatenated to 'rl:' to create
                        a valid ratelimit cache key.
    :returns: None
    """
    rl_key = 'rl:' + incmng_addr
    if cache.get(rl_key):
        log.info(
            'Deleting cache key '
            + rl_key
            + ' at '
            + str(datetime.now()))
        cache.delete(rl_key)
    else:
        log.debug('Key ' + rl_key + ' was not found in the cache.')
        return


def get_num_attempts(incmng_addr):
    """
    Returns the number of requests made from a particular address.

    :param incmng_addr: The address to be concatenated to 'rl:' to create
                        a valid ratelimit cache key.
    :returns: The cache data stored in a particular ratelimit key,
              specifically the number of requests made.
    :rtype: str
    """

    rl_key = 'rl:' + incmng_addr
    if cache.get(rl_key):
        log.warning("Found RL key.")
        return cache.get(rl_key)
    else:
        log.warning("Did not find RL key.")
        log.debug('Key ' + rl_key + ' was not found in the cache.')
        return
