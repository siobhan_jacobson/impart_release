# -*- coding: utf-8 -*-
"""
Routing for the signed_link application.

.. moduleauthor: CLH
.. versionadded: Acai_Berry
"""

from django.conf.urls import url

from signed_link.views import SignedURLRedirectView

app_name = 'signed'

urlpatterns = [
    url(
        r'^(?P<url>.*\w+)/$',
        SignedURLRedirectView.as_view(),
        name='signed_url'
    ),
]
