# -*- coding: utf-8 -*-
"""
Run the tests for the Crowd Sourcing Open app.

.. moduleauthor:: sba
.. moduleauthor:: saj
.. versionadded:: Acai_Berry
"""

import logging
import os

from django.core.urlresolvers import reverse
from django.test import Client
from django.test import TestCase
from django.test.client import RequestFactory
from email_utils.constants import OPEN_CONTACT_MESSAGE_NOTIFY
from email_utils.constants import OPEN_QUEST_TO_APPROVE_NOTIFY
from email_utils.constants import OPEN_NEW_QUESTION_NOTIFY

from email_utils.models import EmailNotification
from utils.utils import generate_random_hash

from .models import ContactMessage
from .models import FAQ
from .models import ResearchQuestion
from .models import ResearchQuestionComment
from crowd_sourcing.models import DHMCDeptChoiceField
from crowd_sourcing.models import GeiselDeptChoiceField
from crowd_sourcing.models import AllDeptChoiceField
from crowd_sourcing.models import ResearchQuestionTopicChoiceField
from .models import ResearchQuestionVote
from .forms import ContactMessageForm
from .forms import ResearchQuestionCommentForm
from .forms import ResearchQuestionForm

log = logging.getLogger(__name__)


class ResearchQuestionTopicChoiceFieldTestCase(TestCase):

    """Test the research question topic choice field model."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.field1 = ResearchQuestionTopicChoiceField.objects.create(
            name='Surgery')

    def test_str(self):
        """String representation correct."""
        self.assertEqual(self.field1.__str__(), 'Surgery')

    def test_unicode(self):
        """ Unicode representation correct."""
        self.assertEqual(self.field1.__unicode__(), 'Surgery')

    def test_repr(self):
        """Raw representation correct."""
        self.assertEqual(
            self.field1.__repr__(), '{0}, Surgery'.format(self.field1.id))


class DHMCDeptChoiceFieldTestCase(TestCase):

    """Test the DHMC department choice field model."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.field1 = DHMCDeptChoiceField.objects.create(
            name='Cardiology')

    def test_str(self):
        """String representation correct."""
        self.assertEqual(self.field1.__str__(), 'Cardiology')

    def test_unicode(self):
        """ Unicode representation correct."""
        self.assertEqual(self.field1.__unicode__(), 'Cardiology')

    def test_repr(self):
        """Raw representation correct."""
        self.assertEqual(
            self.field1.__repr__(), '{0}, Cardiology'.format(self.field1.id))


class GeiselDeptChoiceFieldTestCase(TestCase):

    """Test the Geisel department choice field model."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.field1 = GeiselDeptChoiceField.objects.create(
            name='Genetics')

    def test_str(self):
        """String representation correct."""
        self.assertEqual(self.field1.__str__(), 'Genetics')

    def test_unicode(self):
        """ Unicode representation correct."""
        self.assertEqual(self.field1.__unicode__(), 'Genetics')

    def test_repr(self):
        """Raw representation correct."""
        self.assertEqual(
            self.field1.__repr__(), '{0}, Genetics'.format(self.field1.id))


class ResearchQuestionTestCase(TestCase):

    """Test the crowd source research question model."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.factory = RequestFactory()
        self.request = self.factory.get(
            '/impart/crowd_sourcing_open/research_question_list')
        self.topic_surgery = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.default_ip_address = '127.0.0.1'

        self.research_question1 = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution="Dartmouth College / Geisel",
            department_geisel=GeiselDeptChoiceField.objects.create(
                name="Surgery"),
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A good one I hope.",
            need_lab_data=1,
            need_lab_data_specify="Blood samples",
            topic=self.topic_surgery,
            approved=False,
            ip_address=self.default_ip_address
        )
        self.research_question2 = ResearchQuestion.objects.create(
            first_name="Jane",
            last_name="Doe",
            email="jdoe@gmail.com",
            institution="Other",
            department_all=AllDeptChoiceField.objects.create(
                name="Oncology"),
            question="A really great question number 2?",
            population="North American.",
            key_exposure_of_interest="Cell phones.",
            key_outcome_of_interest="A good one I hope.",
            need_lab_data=0,
            topic=ResearchQuestionTopicChoiceField.objects.create(
                name="Other"),
            topic_other="Another topic",
            approved=True,
            ip_address=self.default_ip_address
        )
        self.research_question3 = ResearchQuestion.objects.create(
            first_name="Jane",
            last_name="Doe",
            email="jdoe@gmail.com",
            institution="Dartmouth Hitchcock Medical Center",
            department_dhmc=DHMCDeptChoiceField.objects.create(
                name="Medicine"),
            question="A really great question?",
            population="North American.",
            key_exposure_of_interest="Cell phones.",
            key_outcome_of_interest="A good one I hope.",
            need_lab_data=0,
            topic=self.topic_surgery,
            topic_other="Another topic",
            approved=True,
            ip_address=self.default_ip_address
        )
        self.expected_str_representation_rs1 = 'Here is my research question?'

        self.research_question_comment1 = ResearchQuestionComment.objects.create(
            comment="Here is a test comment.",
            submitted_by="John Doe",
            ip_address="127.0.0.1",
            question=self.research_question2,
            check_key=generate_random_hash()
        )
        self.research_question_comment2 = ResearchQuestionComment.objects.create(
            comment="Here is another test comment.",
            submitted_by="Jack Doe",
            ip_address="127.0.0.1",
            question=self.research_question2,
            check_key=generate_random_hash()
        )
        self.research_question_comment3 = ResearchQuestionComment.objects.create(
            comment="Here is another test comment.",
            submitted_by="Jack Doe",
            ip_address="127.0.0.1",
            question=self.research_question2,
            check_key=generate_random_hash()
        )

    def test_str(self):
        """String representation correct."""
        self.assertEqual(
            self.research_question1.__str__(),
            self.expected_str_representation_rs1)

    def test_repr(self):
        """String representation correct."""
        self.assertEqual(
            self.research_question1.__repr__(),
            "{0}, {1}".format(self.research_question1.id,
                              self.research_question1.question))

        self.assertEqual(
            self.research_question2.__repr__(),
            "{0}, {1}".format(self.research_question2.id,
                              self.research_question2.question))

    def test_unicode(self):
        """Unicode representation correct."""
        self.assertEqual(
            self.research_question1.__unicode__(),
            self.expected_str_representation_rs1)

    def test_get_absolute_url(self):
        """Reverse address should be correct for this object."""
        self.assertEqual(self.research_question1.get_absolute_url(),
                         '/crowd_sourcing_open/list/')

    def test_get_detail_url(self):
        """Reverse address should be correct for this object."""
        self.assertEqual(
            self.research_question1.get_detail_url(),
            '/crowd_sourcing_open/detail/{0}'.format(
                self.research_question1.id))

    def test_get_approved_questions(self):
        """Test getting approved research questions."""

        self.approved_questions = ResearchQuestion.get_approved_questions()
        self.assertEqual(len(self.approved_questions), 2)

        self.approved_questions_surgery = ResearchQuestion.get_approved_questions(
            self.topic_surgery.id)
        self.assertEqual(len(self.approved_questions_surgery), 1)

        self.assertEqual(self.approved_questions.filter(
            question="A really great question number 2?")[0].num_comments, 3)

    def test_approve(self):
        """Test the approving of a research question"""
        self.research_question1.approve()
        self.approved_questions = ResearchQuestion.get_approved_questions()
        self.assertEqual(len(self.approved_questions), 3)


class ResearchQuestionCommentTestCase(TestCase):

    """Test the crowd source research question comment model."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.default_ip_address = '127.0.0.1'

        self.research_question1 = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution="Dartmouth College / Geisel",
            department_geisel=GeiselDeptChoiceField.objects.create(
                name="Surgery"),
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A good one I hope.",
            need_lab_data=1,
            need_lab_data_specify="Blood samples",
            topic=ResearchQuestionTopicChoiceField.objects.create(
                name="Surgery"),
            approved=True,
            ip_address=self.default_ip_address
        )
        self.research_question2 = ResearchQuestion.objects.create(
            first_name="Jane",
            last_name="Doe",
            email="jdoe@gmail.com",
            institution="Dartmouth College / Geisel",
            department_geisel=GeiselDeptChoiceField.objects.create(
                name="Oncology"),
            question="A really great question?",
            population="North American.",
            key_exposure_of_interest="Cell phones.",
            key_outcome_of_interest="A good one I hope.",
            need_lab_data=0,
            topic=ResearchQuestionTopicChoiceField.objects.create(
                name="Other"),
            topic_other="Another topic",
            approved=True,
            ip_address=self.default_ip_address
        )

        self.research_question_comment1 = ResearchQuestionComment.objects.create(
            comment="Here is a test comment.",
            submitted_by="John Doe",
            ip_address="127.0.0.1",
            question=self.research_question1,
            check_key=generate_random_hash()
        )
        self.research_question_comment2 = ResearchQuestionComment.objects.create(
            comment="Here is another really useful test comment.",
            submitted_by="Bob Smith",
            ip_address="127.0.0.1",
            question=self.research_question2,
            check_key=generate_random_hash()
        )
        self.expected_str_representation_rs1 = "Here is a test comment."

    def test_str(self):
        """String representation correct."""
        self.assertEqual(
            self.research_question_comment1.__str__(),
            self.expected_str_representation_rs1)

    def test_repr(self):
        """String representation correct."""
        self.assertEqual(
            self.research_question_comment1.__repr__(),
            "{0}, {1}".format(self.research_question_comment1.id,
                              self.research_question_comment1.comment))

        self.assertEqual(
            self.research_question_comment2.__repr__(),
            "{0}, {1}".format(self.research_question_comment2.id,
                              self.research_question_comment2.comment))

    def test_unicode(self):
        """Unicode representation correct."""
        self.assertEqual(
            self.research_question_comment1.__unicode__(),
            self.expected_str_representation_rs1)

    def test_get_absolute_url(self):
        """Reverse address should be correct for this object."""
        self.assertEqual(self.research_question_comment1.get_absolute_url(),
                         '/crowd_sourcing_open/detail/{0}'.format(
                             self.research_question1.id))


class ResearchQuestionVoteTestCase(TestCase):

    """Test the crowd source research question vote model."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.default_ip_address = '127.0.0.1'
        self.repeating_check_key = generate_random_hash()

        self.research_question1 = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution="Dartmouth College / Geisel",
            department_geisel=GeiselDeptChoiceField.objects.create(
                name="Surgery"),
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A good one I hope.",
            need_lab_data=1,
            need_lab_data_specify="Blood samples",
            topic=ResearchQuestionTopicChoiceField.objects.create(
                name="Surgery"),
            approved=True,
            ip_address=self.default_ip_address
        )
        self.research_question2 = ResearchQuestion.objects.create(
            first_name="Jane",
            last_name="Doe",
            email="jdoe@gmail.com",
            institution="Dartmouth College / Geisel",
            department_geisel=GeiselDeptChoiceField.objects.create(
                name="Oncology"),
            question="A really great question?",
            population="North American.",
            key_exposure_of_interest="Cell phones.",
            key_outcome_of_interest="A good one I hope.",
            need_lab_data=0,
            topic=ResearchQuestionTopicChoiceField.objects.create(
                name="Other"),
            topic_other="Another topic",
            approved=True,
            ip_address=self.default_ip_address
        )

        self.research_question_vote1 = ResearchQuestionVote.objects.create(
            ip_address=self.default_ip_address,
            vote=True,
            question=self.research_question1,
            check_key=generate_random_hash()
        )
        self.research_question_vote2 = ResearchQuestionVote.objects.create(
            ip_address=self.default_ip_address,
            vote=True,
            question=self.research_question2,
            check_key=self.repeating_check_key
        )
        self.expected_str_representation_rs1 = "True"

    def test_str(self):
        """String representation correct."""
        self.assertEqual(
            self.research_question_vote1.__str__(),
            self.expected_str_representation_rs1)

    def test_repr(self):
        """String representation correct."""
        self.assertEqual(
            self.research_question_vote1.__repr__(),
            "{0}, {1}".format(self.research_question_vote1.id,
                              self.research_question_vote1.vote))

        self.assertEqual(
            self.research_question_vote2.__repr__(),
            "{0}, {1}".format(self.research_question_vote2.id,
                              self.research_question_vote2.vote))

    def test_unicode(self):
        """Unicode representation correct."""
        self.assertEqual(
            self.research_question_vote1.__unicode__(),
            self.expected_str_representation_rs1)


class ContactMessageTestCase(TestCase):

    """Test the contact message model."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.default_ip_address = '127.0.0.1'

        self.contact_message1 = ContactMessage.objects.create(
            name="John Doe",
            email_address="jd@gmail.com",
            subject="Test contact message.",
            message="Here is a test contact message.",
            cc_myself=True,
            ip_address=self.default_ip_address
        )

    def test_str(self):
        """String representation correct."""
        self.assertEqual(
            self.contact_message1.__str__(),
            self.contact_message1.message)

    def test_repr(self):
        """String representation correct."""
        self.assertEqual(
            self.contact_message1.__repr__(),
            "{0}, {1}".format(
                self.contact_message1.id,
                self.contact_message1.message))

    def test_unicode(self):
        """Unicode representation correct."""
        self.assertEqual(
            self.contact_message1.__unicode__(),
            self.contact_message1.message)


class FAQTestCase(TestCase):

    """Test the FAQ model."""

    def setUp(self):
        """Create some default objects to use for testing."""

        self.faq1 = FAQ.objects.create(
            question="Where am I?",
            answer="At a computer.",
        )

    def test_str(self):
        """String representation correct."""
        self.assertEqual(self.faq1.__str__(), self.faq1.question)

    def test_repr(self):
        """String representation correct."""
        self.assertEqual(
            self.faq1.__repr__(),
            "{0}, {1}".format(
                self.faq1.id, self.faq1.question))

    def test_unicode(self):
        """Unicode representation correct."""
        self.assertEqual(
            self.faq1.__unicode__(),
            self.faq1.question)


class ResearchQuestionFormTestCase(TestCase):

    """Test the ResearchQuestionForm."""

    def setUp(self):
        """Create some default objects to use for testing."""
        os.environ['NORECAPTCHA_TESTING'] = 'True'
        self.default_ip_address = '127.0.0.1'

    def tearDown(self):
        """Clean up after the test completes."""
        os.environ['NORECAPTCHA_TESTING'] = 'False'

    def test_validation(self):
        """Check how well the form validates data."""
        self.topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.other_topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Other")

        self.institution = 'Dartmouth College / Geisel'
        self.other_institution = 'Other'
        self.department = GeiselDeptChoiceField.objects.create(
            name="Medicine")
        self.department_all = AllDeptChoiceField.objects.create(
            name="Hospital Medicine")
        self.other_department = GeiselDeptChoiceField.objects.create(
            name="Other")
        self.other_department_all = AllDeptChoiceField.objects.create(
            name="Other")

        # Should fail, blank form
        form_data = {
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'first_name': [u'This field is required.'],
                'last_name': [u'This field is required.'],
                'email': [u'This field is required.'],
                'institution': [u'This field is required.'],
                'question': [u'This field is required.'],
                'population': [u'This field is required.'],
                'key_exposure_of_interest': [u'This field is required.'],
                'key_outcome_of_interest': [u'This field is required.'],
                'need_lab_data': [u'This field is required.'],
                'topic': [u'This field is required.'],
                'captcha': [u'This field is required.'],
                'ip_address': [u'This field is required.']
            })

        # Should fail, missing required fields
        form_data = {
            'first_name': 'X' * 50,
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'last_name': [u'This field is required.'],
                'email': [u'This field is required.'],
                'institution': [u'This field is required.'],
                'question': [u'This field is required.'],
                'population': [u'This field is required.'],
                'key_exposure_of_interest': [u'This field is required.'],
                'key_outcome_of_interest': [u'This field is required.'],
                'need_lab_data': [u'This field is required.'],
                'topic': [u'This field is required.'],
                'captcha': [u'This field is required.'],
                'ip_address': [u'This field is required.']
            })

        # Should pass, valid form
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Should pass, valid form with other topic
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.other_topic.id,
            'topic_other': 'another topic',
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Should pass, valid form with specified lab data
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 1,
            'need_lab_data_specify': 'blood samples',
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Should pass, valid form with other institution
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.other_institution,
            'institution_other': 'Kmart',
            'department_all': self.department_all.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 1,
            'need_lab_data_specify': 'blood samples',
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Should pass, valid form with other department
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.other_department.id,
            'department_other': 'Mail room',
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 1,
            'need_lab_data_specify': 'blood samples',
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Should fail, first name is too long
        form_data = {
            'first_name': 'X' * 51,
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'first_name': [u'Ensure this value has at most 50 characters (it has 51).'],
            })

        # Should fail, last name is too long
        form_data = {
            'first_name': 'John',
            'last_name': 'X' * 51,
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'last_name': [u'Ensure this value has at most 50 characters (it has 51).'],
            })

        # Should fail, invalid email
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'blah',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'email': [u'Enter a valid email address.'],
            })

        # Should fail, institution_other is too long
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.other_institution,
            'institution_other': 'X' * 51,
            'department_all': self.department_all.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'institution_other':
                [u'Ensure this value has at most 50 characters (it has 51).'],
            })

        # Should fail, department_other is too long
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.other_department.id,
            'department_other': 'X' * 51,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'department_other':
                [u'Ensure this value has at most 50 characters (it has 51).'],
            })

        # Should fail, question is too long
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'X' * 1001,
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'question':
                [
                    u'Ensure this value has at most 1000 characters (it has 1001).'],
            })

        # Should fail, population is too long
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'X' * 1001,
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'population':
                [
                    u'Ensure this value has at most 1000 characters (it has 1001).'],
            })

        # Should fail, key_exposure_of_interest is too long
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'X' * 1001,
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'key_exposure_of_interest':
                [
                    u'Ensure this value has at most 1000 characters (it has 1001).'],
            })

        # Should fail, key_outcome_of_interest is too long
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'X' * 1001,
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'key_outcome_of_interest':
                [
                    u'Ensure this value has at most 1000 characters (it has 1001).'],
            })

        # Should fail, need_lab_data_specify is too long
        # This form is passing for some reason.
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 1,
            'need_lab_data_specify': 'X' * 1001,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'need_lab_data_specify':
                [
                    u'Ensure this value has at most 1000 characters (it has 1001).'],
            })

        # Should fail, has institution but is missing department
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'department_geisel': [u'This field is required.'],
            })

        # Broken after breaking the department into 3 lists
        # Should fail, department is invalid
        # form_data = {
        #     'first_name': 'John',
        #     'last_name': 'Smith',
        #     'email': 'jsmith@gmail.com',
        #     'institution': self.institution,
        #     'department_geisel': 99,
        #     'question': 'test question?',
        #     'population': 'everyone',
        #     'key_exposure_of_interest': 'cell phones',
        #     'need_lab_data': 0,
        #     'topic': self.topic.id,
        #     'g-recaptcha-response': 'PASSED',
        #     'ip_address': self.default_ip_address
        # }
        # form = ResearchQuestionForm(data=form_data)
        # self.assertFalse(form.is_valid())
        # self.assertEqual(
        #     form.errors,
        #     {
        #         'department_geisel':
        #         [u'Select a valid choice. That choice is not one of the available choices.'],
        #     })

        # Should fail, topic is invalid
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': 99,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'topic':
                [u'Select a valid choice. That choice is not one of the available choices.'],
            })

        # Should fail, invalid institution
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': 'Wrong Institution',
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'institution':
                [u'Select a valid choice. Wrong Institution is not one of the available choices.'],
            })

        # Should fail, missing 'other' and 'specify' fields
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.other_institution,
            'department_all': self.other_department_all.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 1,
            'topic': self.other_topic.id,
            'g-recaptcha-response': 'PASSED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'institution_other': [u'This field is required.'],
                'department_other': [u'This field is required.'],
                'topic_other': [u'This field is required.'],
                'need_lab_data_specify': [u'This field is required.']
            })

        os.environ['NORECAPTCHA_TESTING'] = 'False'
        # Should fail, captcha is wrong
        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'an outcome',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'recaptcha_response_field': 'FAILED',
            'ip_address': self.default_ip_address
        }
        form = ResearchQuestionForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'captcha': [u'This field is required.']
            })


class ResearchQuestionCommentFormTestCase(TestCase):

    """Test the ResearchQuestionCommentForm."""

    def setUp(self):
        """Create some default objects to use for testing."""
        os.environ['NORECAPTCHA_TESTING'] = 'True'

        self.default_ip_address = '127.0.0.1'

        self.topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.institution = 'Dartmouth College / Geisel'
        self.department = GeiselDeptChoiceField.objects.create(
            name="Medicine")

        self.research_question1 = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution=self.institution,
            department_geisel=self.department,
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A potential outcome.",
            need_lab_data=0,
            topic=self.topic,
            approved=True,
            ip_address=self.default_ip_address
        )

    def tearDown(self):
        """Clean up after the test completes."""
        os.environ['NORECAPTCHA_TESTING'] = 'False'

    def test_validation(self):
        """Check how well the form validates data."""
        self.check_key = generate_random_hash()

        # Should fail, blank form
        # os.environ['NORECAPTCHA_TESTING'] = 'False'

        form_data = {
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'comment': [u'This field is required.'],
                'submitted_by': [u'This field is required.'],
                'ip_address': [u'This field is required.'],
                'question': [u'This field is required.'],
                'check_key': [u'This field is required.'],
                'captcha': [u'This field is required.']
            })

        # Should fail, missing fields
        form_data = {
            'comment': 'Here is a comment.'
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertEqual(
            form.errors,
            {
                'submitted_by': [u'This field is required.'],
                'ip_address': [u'This field is required.'],
                'question': [u'This field is required.'],
                'check_key': [u'This field is required.'],
                'captcha': [u'This field is required.']
            })

        # os.environ['NORECAPTCHA_TESTING'] = 'True'

        # Should pass, valid form
        form_data = {
            'comment': 'Here is a comment.',
            'submitted_by': 'Bob Smith',
            'ip_address': self.default_ip_address,
            'question': self.research_question1.id,
            'check_key': generate_random_hash(),
            'g-recaptcha-response': 'PASSED',
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Should fail, comment is too long
        form_data = {
            'comment': 'X' * 1001,
            'submitted_by': 'Bob Smith',
            'ip_address': self.default_ip_address,
            'question': self.research_question1.id,
            'check_key': generate_random_hash(),
            'g-recaptcha-response': 'PASSED',
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'comment':
                [
                    u'Ensure this value has at most 1000 characters (it has 1001).'],
            })

        # Should fail, submitted_by is too long
        form_data = {
            'comment': 'Here is a comment',
            'submitted_by': 'X' * 51,
            'ip_address': self.default_ip_address,
            'question': self.research_question1.id,
            'check_key': generate_random_hash(),
            'g-recaptcha-response': 'PASSED'
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'submitted_by':
                [u'Ensure this value has at most 50 characters (it has 51).'],
            })

        # Should fail, IP is invalid
        form_data = {
            'comment': 'Here is a comment',
            'submitted_by': 'Bob Smith',
            'ip_address': 'abcdef',
            'question': self.research_question1.id,
            'check_key': generate_random_hash(),
            'g-recaptcha-response': 'PASSED'
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'ip_address': [u'Enter a valid IPv4 or IPv6 address.'],
            })

        # Should fail, question ID is invalid
        form_data = {
            'comment': 'Here is a comment',
            'submitted_by': 'Bob Smith',
            'ip_address': self.default_ip_address,
            'question': 8,
            'check_key': generate_random_hash(),
            'g-recaptcha-response': 'PASSED'
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'question':
                [u'Select a valid choice. That choice is not one of the available choices.'],
            })

        os.environ['NORECAPTCHA_TESTING'] = 'False'
        # Should fail, Invalid captcha
        form_data = {
            'comment': 'Here is a comment',
            'submitted_by': 'Bob Smith',
            'ip_address': self.default_ip_address,
            'question': self.research_question1.id,
            'check_key': generate_random_hash(),
            'recaptcha_response_field': 'FAILED'
        }
        form = ResearchQuestionCommentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'captcha': [u'This field is required.']
            })


class ContactMessageFormTestCase(TestCase):

    """Test the Contact Message form."""

    def setUp(self):
        """Create some default objects to use for testing."""
        os.environ['NORECAPTCHA_TESTING'] = 'True'

        self.default_ip_address = '127.0.0.1'

    def tearDown(self):
        """Clean up after the test completes."""
        os.environ['NORECAPTCHA_TESTING'] = 'False'

    def test_validation(self):
        """Check how well the form validates data."""

        # Should fail, blank form
        form_data = {
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'name': [u'This field is required.'],
                'email_address': [u'This field is required.'],
                'subject': [u'This field is required.'],
                'message': [u'This field is required.'],
                'ip_address': [u'This field is required.'],
                'captcha': [u'This field is required.']
            })

        # Should fail, missing fields
        form_data = {
            'name': 'Bruce Banner'
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'email_address': [u'This field is required.'],
                'subject': [u'This field is required.'],
                'message': [u'This field is required.'],
                'ip_address': [u'This field is required.'],
                'captcha': [u'This field is required.']
            })

        # Should pass, valid form
        form_data = {
            'name': 'Bruce Banner',
            'email_address': 'smash@gmail.com',
            'subject': 'How do I submit my order?',
            'message': 'Where do I go to order my lunch?',
            'ip_address': self.default_ip_address,
            'g-recaptcha-response': 'PASSED'
        }
        form = ContactMessageForm(data=form_data)
        self.assertTrue(form.is_valid())

        # Should fail, name is too long
        form_data = {
            'name': 'X' * 101,
            'email_address': 'smash@gmail.com',
            'subject': 'How do I submit my order?',
            'message': 'Where do I go to order my lunch?',
            'ip_address': self.default_ip_address,
            'g-recaptcha-response': 'PASSED'
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'name': [u'Ensure this value has at most 100 characters (it has 101).']
            }
        )

        # Should fail, email_address is invalid
        form_data = {
            'name': 'Bruce Banner',
            'email_address': 'abcdef',
            'subject': 'How do I submit my order?',
            'message': 'Where do I go to order my lunch?',
            'ip_address': self.default_ip_address,
            'g-recaptcha-response': 'PASSED'
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'email_address': [u'Enter a valid email address.'],
            })

        # Should fail, subject is too long
        form_data = {
            'name': 'Bruce Banner',
            'email_address': 'smash@gmail.com',
            'subject': 'X' * 101,
            'message': 'Where do I go to order my lunch?',
            'ip_address': self.default_ip_address,
            'g-recaptcha-response': 'PASSED'
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'subject': [u'Ensure this value has at most 100 characters (it has 101).'],
            })

        # Should fail, message is too long
        form_data = {
            'name': 'Bruce Banner',
            'email_address': 'smash@gmail.com',
            'subject': 'How do I submit my order?',
            'message': 'X' * 1001,
            'ip_address': self.default_ip_address,
            'g-recaptcha-response': 'PASSED'
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'message': [u'Ensure this value has at most 1000 characters (it has 1001).'],
            })

        # Should fail, invalid IP address
        form_data = {
            'name': 'Bruce Banner',
            'email_address': 'smash@gmail.com',
            'subject': 'How do I submit my order?',
            'message': 'Where do I go to order my lunch?',
            'ip_address': 'abcdef',
            'g-recaptcha-response': 'PASSED'
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'ip_address': [u'Enter a valid IPv4 or IPv6 address.'],
            })

        # Should fail, invalid captcha
        os.environ['NORECAPTCHA_TESTING'] = 'False'
        form_data = {
            'name': 'Bruce Banner',
            'email_address': 'smash@gmail.com',
            'subject': 'How do I submit my order?',
            'message': 'Where do I go to order my lunch?',
            'ip_address': self.default_ip_address,
            'recaptcha_response_field': 'FAILED'
        }
        form = ContactMessageForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors,
            {
                'captcha': [u'This field is required.']
            })


class ResearchQuestionListViewTestCase(TestCase):

    """Test the list view."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.client = Client()

        self.default_ip_address = '127.0.0.1'

        self.topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.institution = 'Dartmouth College / Geisel'
        self.department = GeiselDeptChoiceField.objects.create(
            name="Medicine")

        self.research_question = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution=self.institution,
            department_geisel=self.department,
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A potential outcome.",
            need_lab_data=0,
            topic=self.topic,
            approved=True,
            ip_address=self.default_ip_address
        )

    def test_research_question_list_view(self):
        """Test the list view returns 200."""
        response = self.client.get(reverse('crowd_sourcing_open:list'))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(
            response, 'shared/research_question_list.html')
        self.assertContains(response, 'RESEARCH QUESTIONS')
        self.assertEqual(len(response.context[-1]['object_list']), 1)

        response = self.client.get(reverse(
            'crowd_sourcing_open:list'), {'topic_filter': 'all'})
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse(
            'crowd_sourcing_open:list'), {'sort_type': 'votes'})
        self.assertEqual(response.status_code, 200)

        response = self.client.get(reverse(
            'crowd_sourcing_open:list'), {'sort_type': 'views'})
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(
            response, 'shared/research_question_list.html')
        self.assertContains(response, 'RESEARCH QUESTIONS')


class ResearchQuestionDetailViewTestCase(TestCase):

    """Test the detail view."""

    def setUp(self):
        """Create some default objects to use for testing."""

        os.environ['NORECAPTCHA_TESTING'] = 'True'

        self.client = Client()
        self.default_ip_address = '127.0.0.1'

        self.topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.institution = 'Dartmouth College / Geisel'
        self.department = GeiselDeptChoiceField.objects.create(
            name="Medicine")

        self.research_question = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution=self.institution,
            department_geisel=self.department,
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A potential outcome.",
            need_lab_data=0,
            topic=self.topic,
            approved=True,
            ip_address=self.default_ip_address
        )

        self.research_question1 = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution=self.institution,
            department_geisel=self.department,
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A potential outcome.",
            need_lab_data=0,
            topic=self.topic,
            approved=True,
            ip_address=self.default_ip_address
        )

    def tearDown(self):
        """Clean up after the test completes."""
        os.environ['NORECAPTCHA_TESTING'] = 'False'

    def test_research_question_detail_view_get(self):
        """Test the detail view returns 200."""
        response = self.client.get(
            reverse('crowd_sourcing_open:comment_create',
                    kwargs={'pk': self.research_question.id}))
        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(
            response, 'shared/research_question_detail.html')
        self.assertContains(response, 'RESEARCH QUESTION DETAILS')

    def test_research_question_detail_view_post(self):
        """
        Test the post comment create view returns 302 and redirects back to
        the detail/comment page.  Also make sure this is an additional record
        in the table after successfully submitting the form.
        """

        # Get the page once to trigger the set test cookie
        response = self.client.get(
            reverse('crowd_sourcing_open:comment_create',
                    kwargs={'pk': self.research_question.id}))
        self.assertEqual(response.status_code, 200)

        rec_comment_count = ResearchQuestionComment.objects.count()
        rec_vote_count = ResearchQuestionVote.objects.count()

        # Submit a comment without a vote
        form_data = {
            'comment': 'Here is a comment.',
            'submitted_by': 'Bob Smith',
            'ip_address': self.default_ip_address,
            'question': self.research_question.id,
            'check_key': '',
            'g-recaptcha-response': 'PASSED',
        }
        response = self.client.post(
            reverse('crowd_sourcing_open:comment_create',
                    kwargs={'pk': self.research_question.id}), form_data)

        self.assertEqual(
            ResearchQuestionComment.objects.count(), rec_comment_count + 1)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,
                             reverse('crowd_sourcing_open:comment_create',
                                     kwargs={'pk': self.research_question.id}))

        # Submit a comment with a vote
        form_data = {
            'comment': 'Here is a comment.',
            'submitted_by': 'Bob Smith',
            'ip_address': self.default_ip_address,
            'question': self.research_question.id,
            'check_key': '',
            'vote': 1,
            'g-recaptcha-response': 'PASSED',
        }
        response = self.client.post(
            reverse('crowd_sourcing_open:comment_create',
                    kwargs={'pk': self.research_question.id}), form_data)

        self.assertEqual(
            ResearchQuestionComment.objects.count(), rec_comment_count + 2)
        self.assertEqual(
            ResearchQuestionVote.objects.count(), rec_vote_count + 1)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,
                             reverse('crowd_sourcing_open:comment_create',
                                     kwargs={'pk': self.research_question.id}))

        # Submit a comment with a different vote
        form_data = {
            'comment': 'Here is a comment.',
            'submitted_by': 'Bob Smith',
            'ip_address': self.default_ip_address,
            'question': self.research_question.id,
            'vote': 0,
            'g-recaptcha-response': 'PASSED',
        }
        response = self.client.post(
            reverse('crowd_sourcing_open:comment_create',
                    kwargs={'pk': self.research_question.id}), form_data)

        self.assertEqual(
            ResearchQuestionComment.objects.count(), rec_comment_count + 3)
        # Vote count should not go up.
        self.assertEqual(
            ResearchQuestionVote.objects.count(), rec_vote_count + 1)

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response,
                             reverse('crowd_sourcing_open:comment_create',
                                     kwargs={'pk': self.research_question.id}))

        # Test invalid form
        form_data = {
            'comment': 'Here is a comment.',
            'ip_address': self.default_ip_address,
            'question': self.research_question.id,
            'check_key': '',
            'vote': 1,
            'g-recaptcha-response': 'PASSED',
        }
        response = self.client.post(
            reverse('crowd_sourcing_open:comment_create',
                    kwargs={'pk': self.research_question.id}), form_data)

        self.assertEqual(
            ResearchQuestionComment.objects.count(), rec_comment_count + 3)

        self.assertEqual(response.status_code, 200)


class ResearchQuestionCreateViewTestCase(TestCase):

    """Test the create view."""

    def setUp(self):
        """Create some default objects to use for testing."""
        os.environ['NORECAPTCHA_TESTING'] = 'True'

        self.client = Client()
        self.topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.institution = 'Dartmouth College / Geisel'
        self.department = GeiselDeptChoiceField.objects.create(
            name="Medicine")

        EmailNotification.objects.create(
            notification_type=OPEN_QUEST_TO_APPROVE_NOTIFY,
            alt_email='test@localhost',
            extra=0
        )

    def tearDown(self):
        """
        Clean up after the test completes.
        """

        os.environ['NORECAPTCHA_TESTING'] = 'False'

    def test_research_question_create_view_get(self):
        """Test the get create view returns 200."""
        self.response = self.client.get(reverse('crowd_sourcing_open:create'))
        self.assertEqual(self.response.status_code, 200)

        self.assertTemplateUsed(
            self.response, 'shared/research_question_create.html')

    def test_research_question_create_view_post(self):
        """
        Test the post create view returns 302 and redirects to the thank you
        page. Also make sure there is an additional record in the table after
        successfully submitting the form.
        """

        rec_count = ResearchQuestion.objects.count()

        form_data = {
            'first_name': 'John',
            'last_name': 'Smith',
            'email': 'jsmith@gmail.com',
            'institution': self.institution,
            'department_geisel': self.department.id,
            'question': 'test question?',
            'population': 'everyone',
            'key_exposure_of_interest': 'cell phones',
            'key_outcome_of_interest': 'A potential outcome.',
            'need_lab_data': 0,
            'topic': self.topic.id,
            'receive_notifications': 'on',
            'g-recaptcha-response': 'PASSED'
        }

        response = self.client.post(
            reverse('crowd_sourcing_open:create'), form_data)

        self.assertEqual(ResearchQuestion.objects.count(), rec_count + 1)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('crowd_sourcing_open:thank_you'))


class ResearchQuestionApprovalUpdateViewTestCase(TestCase):

    """Test approving questions"""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.client = Client()

        self.default_ip_address = '127.0.0.1'

        self.topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.institution = 'Dartmouth College / Geisel'
        self.department = GeiselDeptChoiceField.objects.create(
            name="Medicine")

        # Long question to test Twitter truncation
        self.research_question = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution=self.institution,
            department_geisel=self.department,
            question="Here is my research question?" * 4,
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A potential outcome.",
            need_lab_data=0,
            topic=self.topic,
            approved=True,
            ip_address=self.default_ip_address
        )

    def test_research_question_approval_update_view(self):
        """Test the research question approval update view."""
        response = self.client.get(
            reverse('crowd_sourcing_open:approve',
                    kwargs={'pk': self.research_question.id}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            'Click the confirm button below to approve this question')

    def test_research_question_approval_update_view_post(self):
        """Test posting to the research question approval update view."""
        response = self.client.post(
            reverse('crowd_sourcing_open:approve',
                    kwargs={'pk': self.research_question.id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('crowd_sourcing_open:list'))


class HomePageDetailViewTestCase(TestCase):

    """Test the latest question displayed on the home page."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.client = Client()
        self.default_ip_address = '127.0.0.1'

        self.topic = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.institution = 'Dartmouth College / Geisel'
        self.department = GeiselDeptChoiceField.objects.create(
            name="Medicine")

        self.research_question1 = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution=self.institution,
            department_geisel=self.department,
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            key_outcome_of_interest="A potential outcome.",
            need_lab_data=0,
            topic=self.topic,
            approved=True,
            ip_address=self.default_ip_address
        )

    def test_home_page_detail_view_get(self):
        """Test the get get view returns 200."""
        # self.response = self.client.get(reverse('index'))
        self.response = self.client.get(reverse('crowd_sourcing_open:index'))
        self.assertEqual(self.response.status_code, 200)

        # self.assertTemplateUsed(
        #     self.response, 'index.html')
        self.assertTemplateUsed(
            self.response, 'crowd_sourcing_open/index.html')


class ContactMessageCreateViewTestCase(TestCase):

    """Test the contact message create view."""

    def setUp(self):
        """Create some default objects to use for testing."""
        os.environ['NORECAPTCHA_TESTING'] = 'True'

        self.client = Client()
        self.default_ip_address = '127.0.0.1'

        EmailNotification.objects.create(
            notification_type=OPEN_CONTACT_MESSAGE_NOTIFY,
            alt_email='test@localhost',
            extra=0
        )

    def tearDown(self):
        """
        Clean up after the test completes.
        """

        os.environ['NORECAPTCHA_TESTING'] = 'False'

    def test_contact_message_create_view_get(self):
        """Test the get create view returns 200."""
        self.response = self.client.get(reverse('crowd_sourcing_open:contact'))
        self.assertEqual(self.response.status_code, 200)

        self.assertTemplateUsed(
            self.response, 'shared/contact_form.html')

    def test_contact_create_view_post(self):
        """
        Test the post create view returns 302 and redirects contact message
        thank you page. Also make sure this is an additional record in the
        table after successfully submitting the form.
        """

        rec_count = ContactMessage.objects.count()

        form_data = {
            'name': 'Bruce Banner',
            'email_address': 'test@localhost',
            'subject': 'How do I submit my order?',
            'message': 'Where do I go to order my lunch?',
            'ip_address': self.default_ip_address,
            'cc_myself': True,
            'g-recaptcha-response': 'PASSED'
        }

        response = self.client.post(
            reverse('crowd_sourcing_open:contact'), form_data)

        self.assertEqual(ContactMessage.objects.count(), rec_count + 1)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse(
            'crowd_sourcing_open:thank_you_contact'))


class FAQViewTestCase(TestCase):

    """Test the FAQ view."""

    def setUp(self):
        """Create some default objects to use for testing."""

        self.client = Client()

    def test_faq_view_get(self):
        """Test the get create view returns 200."""
        self.response = self.client.get(reverse('crowd_sourcing_open:faq'))
        self.assertEqual(self.response.status_code, 200)

        self.assertTemplateUsed(
            self.response, 'shared/faq.html')


class UnsubscribeEmailUpdateViewTestCase(TestCase):

    """Test unsubscribing email."""

    def setUp(self):
        """Create some default objects to use for testing."""
        self.client = Client()

        self.default_ip_address = '127.0.0.1'

        self.email_notification = EmailNotification.objects.create(
            notification_type=OPEN_NEW_QUESTION_NOTIFY,
            alt_email='test@localhost',
            extra=0
        )

    def test_unsubscribe_email_update_view(self):
        """Test the research question approval update view."""
        response = self.client.get(
            reverse('crowd_sourcing_open:unsubscribe',
                    kwargs={'pk': self.email_notification.id}))
        self.assertEqual(response.status_code, 200)
        self.assertContains(
            response,
            'Click the confirm button below to remove this email address from the Impart New Submitted Question Notification List:')

    def test_unsubscribe_email_update_view_post(self):
        """Test posting to the research question approval update view."""
        response = self.client.post(
            reverse('crowd_sourcing_open:unsubscribe',
                    kwargs={'pk': self.email_notification.id}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('crowd_sourcing_open:unsubscribe_confirm'))
