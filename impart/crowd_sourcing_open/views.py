# -*- coding: utf-8 -*-
"""
Views for the Crowd Sourcing app.

.. moduleauthor:: saj
.. versionadded:: Acai_Berry
"""

import logging

from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect

from django.shortcuts import get_object_or_404
from django.template.response import ContentNotRenderedError
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import UpdateView

import email_utils.constants
from email_utils.models import ImpartMailer
from email_utils.models import EmailNotification
from utils.mixins import ChangeActionMessageMixin
from utils.utils import generate_random_hash
from utils.utils import get_client_ip
from utils.utils import get_cookie
from utils.utils import set_cookie

from .forms import ContactMessageForm
from .forms import ResearchQuestionCommentForm
from .forms import ResearchQuestionForm
from .models import ContactMessage
from .models import FAQ
from .models import ResearchQuestion
from .models import ResearchQuestionComment
from .models import ResearchQuestionTopicChoiceField
from .models import ResearchQuestionVote

log = logging.getLogger(__name__)


class ResearchQuestionCreateView(CreateView):
    """Allow a user to submit a crowd sourcing research question."""

    model = ResearchQuestion
    form_class = ResearchQuestionForm
    template_name = 'shared/research_question_create.html'
    success_url = reverse_lazy('crowd_sourcing_open:thank_you')

    def form_valid(self, form):
        """
        Extend so we can send an email to staff to approve the question.

        :param ModelForm form: The form associated with this view.
        :returns: The destination to which the user should be sent.
        :rtype: HttpResponse
        """

        data = form.save()

        ImpartMailer().crowd_source_email_for_approval(
            data, self.request, 'crowd_sourcing_open')
        log.info(
            'New research question submitted (ID: {0}) by user from IP address: {1} and notification email sent'.format(
                data.id, get_client_ip(self.request)))
        return super(
            ResearchQuestionCreateView, self).form_valid(form)

    def post(self, *args, **kwargs):
        """
        Extend to set the IP address in the post array.

        If the user opted for email notifications add her email to
        the OPEN_NEW_QUESTION_NOTIFY list if the email is not
        already in the table.

        :param *args: Optional set of positional arguments to pass to super().
        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: Destination.
        :rtype: HttpResponse or HttpResponseRedirect
        """

        post_data = self.request.POST.copy()
        post_data['ip_address'] = get_client_ip(self.request)

        self.request.POST = post_data

        if self.request.POST.get("receive_notifications"):
            if EmailNotification.objects.filter(
                alt_email=self.request.POST.get("email"),
                notification_type=email_utils.constants.OPEN_NEW_QUESTION_NOTIFY).count() == 0:
                email_notification = EmailNotification(
                    notification_type=email_utils.constants.OPEN_NEW_QUESTION_NOTIFY,
                    alt_email=self.request.POST.get("email"),
                    extra=0)
                email_notification.save()

        return super(
            ResearchQuestionCreateView, self).post(*args, **kwargs)


class ResearchQuestionListView(ListView):
    """Show a list of submitted research questions."""

    model = ResearchQuestion
    template_name = 'shared/research_question_list.html'
    context_object_name = 'questions'

    def get_queryset(self):
        """
        Get all approved research questions and info for table.

        :returns: queryset of all approved research questions.
        :rtype: QuerySet
        """

        return ResearchQuestion.get_approved_questions()

    def get_context_data(self, **kwargs):
        """
        Add header label to the context.

        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: With header label.
        :rtype: Context
        """

        context = super(
            ResearchQuestionListView, self).get_context_data(**kwargs)

        context['topic_list'] = ResearchQuestionTopicChoiceField.objects.all()
        context['questions'] = self.questions
        context['current_filter'] = self.topic_filter
        context['active_tab'] = self.sort_type

        return context

    def get(self, *args, **kwargs):
        """
        Extend to get the topic to filter on.

        :param *args: Optional set of positional arguments to pass to super().
        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: Destination.
        :rtype: HTTPResponse or HttpResponseRedirect
        """

        self.topic_filter = self.request.GET.get('topic_filter')
        self.sort_type = self.request.GET.get('sort_type')
        if not self.sort_type:
            self.sort_type = 'newest'
        if self.topic_filter == 'all':
            self.topic_filter = None

        self.questions = ResearchQuestion.get_approved_questions(
            self.topic_filter, self.sort_type)

        return super(ResearchQuestionListView, self).get(
            *args, **kwargs)


class ResearchQuestionCommentCreateView(ChangeActionMessageMixin, CreateView):
    """
    Show information about a research question and allow for comment entry.
    """

    model = ResearchQuestionComment
    form_class = ResearchQuestionCommentForm
    template_name = 'shared/research_question_detail.html'
    success_msg = _('Thanks! Your comment was added.')

    def set_test_cookie(self):
        """Set the test cookie in self.request.session."""
        self.request.session.set_test_cookie()

    def check_and_delete_test_cookie(self):
        """
        Make sure the test cookie worked.

        :returns: Whether the test cookie worked.
        :rtype: bool
        """

        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
            return True
        else:
            log.info(
                'User from IP Address {0} does not have cookies enabled'.format(
                    get_client_ip(self.request)))
            return False

    def get_success_url(self):
        """ Provide the success URL with the current question ID."""
        return reverse(
            'crowd_sourcing_open:comment_create', kwargs={'pk': self.question.id})

    def get_context_data(self, **kwargs):
        """
        Get question information.

        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: With header label.
        :rtype: Context
        """

        context = super(
            ResearchQuestionCommentCreateView, self).get_context_data(**kwargs)
        context['question'] = self.question
        context['comments'] = self.comments

        return context

    def post(self, *args, **kwargs):
        """
        Extend to get the question information.

        Get comments for the question to show in the comments table.
        Copy the immutable POST data, set the IP Address and question ID and
        put it back in self.request.POST when the form is submitted.

        Set the check_key cookie if one does not already exist for the
        question.

        Overwrite the vote value in the question object with the value just
        submitted to get around a bug where the vote isn't being displayed on
        the form after returning after a validation error.

        Only save a vote if the comments form posted successfully. This is
        determined with a try/except looking for the ContentNotRenderedError
        exception, which is thrown when the response is accessed after
        an unsuccessful post.

        :param *args: Optional set of positional arguments to pass to super().
        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: Destination.
        :rtype: HttpResponse or HttpResponseRedirect
        """

        self.question_id = self.kwargs.pop('pk')

        self.question = ResearchQuestion.get_question_details(
            self.request, self.question_id)
        self.comments = ResearchQuestionComment.get_comments_for_question(
            question_id=self.question_id)

        # Flag to set check key cookie
        set_check_key_cookie = False

        post_data = self.request.POST.copy()
        post_data['ip_address'] = get_client_ip(self.request)
        post_data['question'] = self.question.id
        # Cookie would already exist in the case where the user
        # has commented on this question already.
        post_data['check_key'] = get_cookie(
            self.request, 'check_key_' + str(self.question.id))

        if post_data['check_key'] or self.check_and_delete_test_cookie():
            if 'vote' in self.request.POST:
                self.question.current_vote = bool(self.request.POST['vote'])

            if post_data['check_key'] is None:
                post_data['check_key'] = generate_random_hash()
                set_check_key_cookie = True

            self.request.POST = post_data

            self.response = super(
                ResearchQuestionCommentCreateView, self).post(
                *args, **kwargs)

            if set_check_key_cookie:
                # Put the matching hash into the cookie
                set_cookie(self.response, 'check_key_' + str(self.question.id),
                           post_data['check_key'], None)

            log.info(
                'User from IP Address: {0} left a comment for question {1}'.format(
                    post_data['ip_address'], post_data['question']))

            # Refer to the method comment.
            try:
                log.info('Post response: {0}'.format(self.response))
            except ContentNotRenderedError:
                log.info('Did not post.')
            else:
                self.question = self.save_vote()

            return self.response
        else:
            # The user fortunately cannot submit a comment without cookies
            # enabled. He will get a Forbidden (403) page with this message:
            # Reason given for failure:
            #    CSRF cookie not set.
            log.info(
                'User from IP Address: {0} attempted to leave a comment for question {1} but did not have cookies enabled'.format(
                    post_data['ip_address'], post_data['question']))

            return super(ResearchQuestionCommentCreateView, self).post(
                *args, **kwargs)

    def get(self, *args, **kwargs):
        """
        Extend to get the question information and set the test cookie.

        Increase the view count for the question by 1.  This currently does not
        attempt to filter view counts from web crawlers.

        :param *args: Optional set of positional arguments to pass to super().
        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: Destination.
        :rtype: HttpResponse or HttpResponseRedirect
        """

        self.set_test_cookie()

        self.question_id = self.kwargs.pop('pk')

        self.question = ResearchQuestion.get_question_details(
            self.request, self.question_id)

        self.comments = ResearchQuestionComment.get_comments_for_question(
            question_id=self.question_id)

        current_question = ResearchQuestion.objects.get(pk=self.question_id)
        current_question.view_count += 1
        current_question.save()

        return super(
            ResearchQuestionCommentCreateView, self).post(*args, **kwargs)

    def save_vote(self):
        """Check the post data for the vote and save."""
        if 'vote' in self.request.POST:
            voted_question = ResearchQuestion.objects.get(
                id=self.question_id)
            try:
                existing_vote = ResearchQuestionVote.objects.get(
                    question_id=self.question_id,
                    check_key=self.request.POST['check_key']
                )
                existing_vote.vote = int(self.request.POST['vote'])
                existing_vote.save()
            except ObjectDoesNotExist:
                new_vote = ResearchQuestionVote.objects.create(
                    question=voted_question,
                    vote=int(self.request.POST['vote']),
                    ip_address=self.request.POST['ip_address'],
                    check_key=self.request.POST['check_key']
                )
                new_vote.save()
        else:
            log.info(
                'User from IP Address: {0} left a comment for question {1} but did not vote'.format(
                    self.request.POST['ip_address'], self.request.POST['question']))


class ResearchQuestionApprovalUpdateView(UpdateView):
    """
    Enable staff to approve a research question.

    Uses the signed_link code to embed the information about redirect
    and question ID.
    """

    model = ResearchQuestion
    # Defining the fields is required in Django 1.8
    # I don't think what I put here matters because I'm handling the
    # database changes manually in the post.
    fields = ['approved']
    success_url = reverse_lazy('crowd_sourcing_open:list')
    template_name = 'shared/research_question_approval.html'

    def get_object(self, queryset=None):
        """
        Extend to pull out the question ID from the URL.

        :param QuerySet: ResearchQuestion queryset for update view.
        :returns: object
        :rtype: ResearchQuestion object
        """

        self.object = super(
            ResearchQuestionApprovalUpdateView, self).get_object(queryset)
        self.question = get_object_or_404(
            ResearchQuestion, id=self.kwargs['pk'],
        )
        log.info(
            'The page to approve question {0} was viewed by user from IP Address {1}'.format(
                self.question.id, get_client_ip(self.request)))
        return self.object

    def post(self, request, *args, **kwargs):
        """
        Extend to approve the question and send out notifications to users.

        :param *args: Optional set of positional arguments to pass to super().
        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: Destination to send the user.
        :rtype: HttpResponse or HttpResponseRedirect
        """

        self.object = self.get_object()
        self.question.approve()

        ImpartMailer().new_approved_question(
            self.question, self.request, 'crowd_sourcing_open')

        log.info(
            'Question {0} was approved by user from IP Address: {1}'.format(
                self.question.id, get_client_ip(self.request)))

        return HttpResponseRedirect(self.get_success_url())


class UnsubscribeEmailUpdateView(UpdateView):
    """
    Allow users to remove their email addresses from the Impart
    New Approved Question Notification List.

    Uses the signed_link code to embed the information about redirect
    and email address.
    """

    model = EmailNotification
    # Defining the fields is required in Django 1.8
    # I don't think what I put here matters because I'm handling the
    # database changes manually in the post.
    fields = ['alt_email']
    success_url = reverse_lazy('crowd_sourcing_open:unsubscribe_confirm')
    template_name = 'shared/unsubscribe_email.html'

    def get_object(self, queryset=None):
        """
        Extend to pull out the email address from the URL.

        :param QuerySet: EmailNotification queryset for update view.
        :returns: object
        :rtype: EmailNotification object
        """

        self.object = super(
            UnsubscribeEmailUpdateView, self).get_object(queryset)
        self.email_notification = get_object_or_404(
            EmailNotification, id=self.kwargs['pk'],
        )
        log.info(
            'The page to unsubscribe an email address, {0}, was viewed by user from IP Address {1}'.format(
                self.email_notification.alt_email, get_client_ip(self.request)))
        return self.object

    def post(self, request, *args, **kwargs):
        """
        Extend to delete the email from the notification list.

        :param *args: Optional set of positional arguments to pass to super().
        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: Destination to send the user.
        :rtype: HttpResponse or HttpResponseRedirect
        """

        self.object = self.get_object()
        self.email_notification.delete()

        log.info(
            'Email address {0} was removed by user from IP Address: {1}'.format(
                self.email_notification.alt_email, get_client_ip(
                    self.request)))

        return HttpResponseRedirect(self.get_success_url())


class ThankYouView(TemplateView):
    """A thank you page for indicating success on form submissions."""

    template_name = 'shared/thank_you_moderated.html'


class UnsubscribeConfirmView(TemplateView):
    """A confirmation page indicating a successfully unsubscribing."""

    template_name = 'shared/unsubscribe_confirmation.html'


class HomePageDetailView(ListView):
    """List the latest approved question."""

    template_name = 'crowd_sourcing_open/index.html'

    def get_queryset(self):
        """
        Get the latest approved research question.

        :returns: queryset of all approved research questions.
        :rtype: QuerySet
        """

        if ResearchQuestion.objects.filter(approved=1):
            questions = ResearchQuestion.objects.filter(
                approved=1).order_by('-created').first()
        else:
            questions = None
        return questions

    def get_context_data(self, **kwargs):
        """
        Get latest approved question and FAQs and put them in the context.

        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: With header label.
        :rtype: Context
        """

        faq = FAQ()

        context = super(
            HomePageDetailView, self).get_context_data(**kwargs)
        context['question'] = self.get_queryset()
        context['faqs'] = faq.get_first_three()

        return context


class ContactMessageCreateView(CreateView):
    """Allow a user to submit a message via the contact form."""

    model = ContactMessage
    form_class = ContactMessageForm
    template_name = 'shared/contact_form.html'
    success_url = reverse_lazy('crowd_sourcing_open:thank_you_contact')

    def form_valid(self, form):
        """
        Extend so we can send an email to staff to approve the question.

        :param ModelForm form: The form associated with this view.
        :returns: The destination to which the user should be sent.
        :rtype: HttpResponse
        """

        data = form.save()

        ImpartMailer().contact_message(
            data, self.request, 'crowd_sourcing_open')
        log.info(
            'New message submitted (ID: {0}) by user from IP address: {1} and email sent'.format(
                data.id, get_client_ip(self.request)))
        return super(
            ContactMessageCreateView, self).form_valid(form)

    def post(self, *args, **kwargs):
        """
        Extend to set the IP address in the post array.

        :param *args: Optional set of positional arguments to pass to super().
        :param **kwargs: Optional set of keyword arguments to pass to super().
        :returns: Destination.
        :rtype: HttpResponse or HttpResponseRedirect
        """

        post_data = self.request.POST.copy()
        post_data['ip_address'] = get_client_ip(self.request)

        self.request.POST = post_data

        return super(
            ContactMessageCreateView, self).post(*args, **kwargs)


class ThankYouContactView(TemplateView):
    """A page for indicating success on contact message submissions."""

    template_name = 'shared/thank_you_contact.html'


class FAQView(ListView):
    """The view for the FAQ page."""

    model = FAQ
    template_name = 'shared/faq.html'
    context_object_name = 'faqs'


class AboutView(TemplateView):
    """The about page."""

    template_name = 'shared/about.html'
