# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import model_utils.fields
import django.utils.timezone
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('crowd_sourcing', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('name', models.CharField(max_length=100)),
                ('email_address', models.EmailField(max_length=254, validators=[django.core.validators.EmailValidator()])),
                ('subject', models.CharField(max_length=100)),
                ('message', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('cc_myself', models.BooleanField(default=False)),
                ('ip_address', models.GenericIPAddressField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='FAQ',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('question', models.CharField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('answer', models.CharField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResearchQuestion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=254, validators=[django.core.validators.EmailValidator()])),
                ('institution', models.CharField(max_length=35, choices=[(b'Dartmouth College / Geisel', b'Dartmouth College / Geisel'), (b'Dartmouth Hitchcock Medical Center', b'Dartmouth Hitchcock Medical Center'), (b'Other', b'Other')], validators=[django.core.validators.MaxLengthValidator(35)])),
                ('institution_other', models.CharField(default=b'', max_length=50, blank=True)),
                ('department_other', models.CharField(default=b'', max_length=50, blank=True)),
                ('question', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('population', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('key_exposure_of_interest', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('key_outcome_of_interest', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('need_lab_data', models.BooleanField(default=0, choices=[(0, b'No, I do not need laboratory or biometric data.'), (1, b'Yes, I do need laboratory or biometric data.')])),
                ('need_lab_data_specify', models.TextField(default=b'', max_length=1000, blank=True, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('topic_other', models.CharField(default=b'', max_length=50, blank=True)),
                ('approved', models.BooleanField(default=False)),
                ('ip_address', models.GenericIPAddressField()),
                ('view_count', models.IntegerField(default=0)),
                ('department_all', models.ForeignKey(related_name='dept_all_open', default=b'', blank=True, to='crowd_sourcing.AllDeptChoiceField', null=True)),
                ('department_dhmc', models.ForeignKey(related_name='dept_dhmc_open', default=b'', blank=True, to='crowd_sourcing.DHMCDeptChoiceField', null=True)),
                ('department_geisel', models.ForeignKey(related_name='department_geisel', default=b'', blank=True, to='crowd_sourcing.GeiselDeptChoiceField', null=True)),
                ('topic', models.ForeignKey(related_name='topic_open', to='crowd_sourcing.ResearchQuestionTopicChoiceField')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResearchQuestionComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('comment', models.TextField(max_length=1000, validators=[django.core.validators.MaxLengthValidator(1000)])),
                ('submitted_by', models.CharField(max_length=50)),
                ('ip_address', models.GenericIPAddressField()),
                ('check_key', models.TextField(max_length=32, validators=[django.core.validators.MaxLengthValidator(32)])),
                ('question', models.ForeignKey(to='crowd_sourcing_open.ResearchQuestion')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ResearchQuestionVote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, verbose_name='created', editable=False)),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, verbose_name='modified', editable=False)),
                ('ip_address', models.GenericIPAddressField()),
                ('vote', models.BooleanField(default=0)),
                ('check_key', models.TextField(max_length=32, validators=[django.core.validators.MaxLengthValidator(32)])),
                ('question', models.ForeignKey(to='crowd_sourcing_open.ResearchQuestion')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
