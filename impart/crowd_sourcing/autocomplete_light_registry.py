from autocomplete_light import shortcuts as autocomplete_light

from .models import ResearchQuestion
from .models import ResearchQuestionTopicChoiceField
from .models import DHMCDeptChoiceField
from .models import GeiselDeptChoiceField
from .models import AllDeptChoiceField

autocomplete_light.register(
    ResearchQuestion,
    # Just like in ModelAdmin.search_fields
    search_fields=[
        'topic',
        'department_dhmc',
        'department_geisel',
        'department_all'],
    attrs={
        # This will set the input placeholder attribute:
        'placeholder': 'Field of Inquiry',
        # This will set the yourlabs.Autocomplete.minimumCharacters
        # options, the naming conversion is handled by jQuery
        'data-autocomplete-minimum-characters': 2,
    },
    # This will set the data-widget-maximum-values attribute on the
    # widget container element, and will be set to
    # yourlabs.Widget.maximumValues (jQuery handles the naming
    # conversion).
    widget_attrs={
        'data-widget-maximum-values': 4,
        # Enable modern-style widget !
        'class': 'modern-style',
    },
)

autocomplete_light.register(
    ResearchQuestionTopicChoiceField,
    search_fields=['^name'],
    attrs={
        'placeholder': 'Start typing Field of Inquiry',
        'data-autocomplete-minimum-characters': 1,
        'class': 'form-control'
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'form-control no_padding',
    },
)

autocomplete_light.register(
    DHMCDeptChoiceField,
    search_fields=['^name'],
    attrs={
        'placeholder': 'Start typing Department',
        'data-autocomplete-minimum-characters': 1,
        'class': 'form-control'
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'form-control no_padding',
    },
)

autocomplete_light.register(
    GeiselDeptChoiceField,
    search_fields=['^name'],
    attrs={
        'placeholder': 'Start typing Department',
        'data-autocomplete-minimum-characters': 1,
        'class': 'form-control'
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'form-control no_padding',
    },
)

autocomplete_light.register(
    AllDeptChoiceField,
    search_fields=['^name'],
    attrs={
        'placeholder': 'Start typing Department',
        'data-autocomplete-minimum-characters': 1,
        'class': 'form-control'
    },
    widget_attrs={
        'data-widget-maximum-values': 4,
        'class': 'form-control no_padding',
    },
)
