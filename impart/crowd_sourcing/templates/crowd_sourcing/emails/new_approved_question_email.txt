A new research question has been submitted via the Impart site:

Question: {{ question.question|safe }}

Please click here to vote and leave comments:  {{ question_link }}

---

Please click here if you'd like to unsubscribe from new question notifications:

{{ unsubscribe_link }}