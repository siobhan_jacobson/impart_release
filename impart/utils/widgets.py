# -*- coding: utf-8 -*-
"""
Collection of custom widgets for use throughout the project.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

import floppyforms.__future__ as forms


class FullNameTextBoxesWidget(forms.widgets.MultiWidget):

    """
    Present two side-by-side textboxes for a user to fill in full name.

    Corresponds to :py:class:`FullNameTextBoxesField` field.
    """

    def __init__(self, attrs=None):
        """
        Initialize some key values, like the sub-widgets.

        :param dict attrs: Optional attributes to pass to super().
        """
        _widgets = (
            forms.widgets.TextInput,
            forms.widgets.TextInput,
        )
        super(FullNameTextBoxesWidget, self).__init__(_widgets, attrs)

    def decompress(self, value):
        """
        Parse data.

        Right now, essentially a pass-through.

        :param value: Object containing the data to parse.
        :returns: Parsed data.
        :rtype: list
        """
        if value:
            return [value[0], value[1]]
        return [None, None]

    def format_output(self, rendered_widgets):
        """
        Combine the sub-widgets.

        Right now, this merely re-implements the default, but it is place
        in case we need to change the formatting.

        :param list rendered_widgets: The sub-widgets as a list of strs.
        :returns: Joined strs of widgets.
        :rtype: str
        """
        return super(FullNameTextBoxesWidget, self).format_output(
            rendered_widgets)

        # TODO: Finish formatting this

        # joined = ''.join(rendered_widgets)
        # tokens = joined.split('\n')
        # if True:
        #     return tokens[0]

        # str = '<label for="first_name">First Name:'
        # str += tokens[0]
        # str += '</label><label for="last_name">Last Name:'
        # str += tokens[1]
        # str += '</label>'

        # return str
