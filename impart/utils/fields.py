# -*- coding: utf-8 -*-
"""
Collection of custom fields for use throughout the project.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

import floppyforms.__future__ as forms

from .widgets import FullNameTextBoxesWidget


class FullNameTextBoxesField(forms.MultiValueField):
    """
    Fields for the two side-by-side textboxes for a user to fill in full name.

    Corresponds to :py:class:`FullNameTextBoxesWidget` widget.
    """

    widget = FullNameTextBoxesWidget

    def __init__(self, *args, **kwargs):
        """
        Initialize some key values, like the sub-widgets.

        :param *args: Any optional positional arguments to pass to super().
        :param **kwargs: Any optional keyword arguments to pass to super().
        """

        fields = (
            forms.CharField(max_length=100, label='First Name'),
            forms.CharField(max_length=100, label='Last Name'),
        )
        super(FullNameTextBoxesField, self).__init__(
            fields, *args, **kwargs)

    def compress(self, data_list):
        """
        Parse data.

        :param list data_list: The data to parse.
        :returns: Parsed data.
        :rtype: list
        """

        return data_list
