# -*- coding: utf-8 -*-
"""
Collection of utilities shared among other applications.

.. moduleauthor:: sba
.. moduleauthor:: saj
.. versionadded: Acai_Berry
"""

from binascii import hexlify
from datetime import datetime
from datetime import timedelta
import logging
from os import urandom
from requests import get as req_get

from django.conf import settings
from django.contrib.auth import get_user_model

log = logging.getLogger(__name__)


def get_client_ip(request):
    """
    Get the IP adress from NGINX proxy.

    Because our system is proxied, the IP address will always
    be that of the proxy server.  To get the actual IP address
    we need a helper function.  However, when testing, we are
    not running behind a proxy server and need a different
    method for getting the IP address.  This function will get
    the ip address from the appropriate place (directly from the
    request object or from http proxy header) and convert
    it to a usable format.

    :param Request request: The request from this IP.
    :returns: The IP address for this request.
    :rtype: str
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def set_cookie(response, key, value, days_expire=7):
    """
    Set a cookie in the response.

    Borrowed from
    http://stackoverflow.com/questions/1622793/django-cookies-how-can-i-set-them

    :param HttpResponse response: The response to set the cookie in.
    :param str key: The key in the cookie for the value.
    :param value: The value to store.
    :param int days_expire: Number of days until the cookie expires.
    """

    if days_expire is None:
        max_age = 365 * 24 * 60 * 60  # one year
    else:
        max_age = days_expire * 24 * 60 * 60

    expires = datetime.strftime(
        datetime.utcnow() +
        timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
    response.set_cookie(key, value, max_age=max_age,
                        expires=expires,
                        domain=settings.SESSION_COOKIE_DOMAIN,
                        secure=settings.SESSION_COOKIE_SECURE or None)


def get_cookie(request, key):
    """
    Get a cookie from the request.

    :param HttpRequest request: The request to get the cookie from.
    :param str key: The key for the value to retrieve.
    :returns: The value of the cookie
    """
    if key in request.COOKIES:
        return request.COOKIES.get(key)
    else:
        return None


def generate_random_hash():
    """
    Generate a random hash.
    """

    return hexlify(urandom(16))


def save_email(backend, user, response, *args, **kwargs):
    """
    Retrieve the user's email from Facebook and save it to the user.

    Save the email if it is different from the email we already have.

    :param backend: The current backend instance.
    :param user: The user instance.
    :param response: The server user-details response
    :param *args: Optional set of positional arguments.
    :param **kwargs: Optional set of keyword arguments.
    """
    if backend.name == 'facebook':
        social_user = user.social_auth.filter(
            provider='Facebook',
        ).first()
        # Get the user's email from Facebook with the access token
        r = req_get(
            'https://graph.facebook.com/{0}?fields=email&access_token={1}'.format(
                social_user.uid,
                social_user.extra_data['access_token']))

        fb_email = r.json()['email']
        current_user = get_user_model().objects.filter(id=user.id).first()
        if not current_user.email or current_user.email != fb_email:
            current_user.email = fb_email
            current_user.save()
