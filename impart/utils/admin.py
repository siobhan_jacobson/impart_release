# -*- coding: utf-8 -*-
"""
Control objects for the Utils application via the admin interface.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

from django.contrib import admin
