# -*- coding: utf-8 -*-
"""
Represent objects which should be available to the entire application.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

# If any database-backed models are added to this, need to run:
#   ./manage.py schemamigration utils --initial
#   and then remove this comment
# Probably best if they are not, however
