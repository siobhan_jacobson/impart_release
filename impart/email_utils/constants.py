# -*- coding: utf-8 -*-
"""
Collection of email constants for use throughout the project.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

# Email notification types
EVENT_CC = 'event_cc'

CONTACT_MESSAGE_NOTIFY = 'contact_message_notify'
NEW_QUESTION_NOTIFY = 'question_notify'

OPEN_QUEST_TO_APPROVE_NOTIFY = 'open_quest_to_approve_notify'
OPEN_CONTACT_MESSAGE_NOTIFY = 'open_contact_message_notify'
OPEN_NEW_QUESTION_NOTIFY = 'open_question_notify'

COMM_QUEST_TO_APPROVE_NOTIFY = 'comm_quest_to_approve_notify'
COMM_CONTACT_MESSAGE_NOTIFY = 'comm_contact_message_notify'
COMM_NEW_QUESTION_NOTIFY = 'comm_question_notify'
