# -*- coding: utf-8 -*-
"""
Utilities for sending email to users within the system.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

from django.contrib import admin

from .models import EmailNotification

admin.site.register(EmailNotification)
