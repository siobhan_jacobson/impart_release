# -*- coding: utf-8 -*-
"""
Run the tests for the Email Utilities application.

.. moduleauthor:: sba
.. versionadded:: Acai_Berry
"""

import os

from django import template
from django.contrib.auth import get_user_model
from django.core import mail
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.test.client import RequestFactory

from crowd_sourcing_open.models import ResearchQuestion
from crowd_sourcing_open.models import GeiselDeptChoiceField
from crowd_sourcing_open.models import ResearchQuestionTopicChoiceField
from signed_link.utilities import CryptLink

from .models import ImpartMailer
from .models import EmailNotification


class EmailNotificationTestCase(TestCase):

    """
    Test the EmailNotification class.
    """

    def setUp(self):
        """
        Create some default objects to use for testing.
        """

        self.user = get_user_model().objects.create_user(
            'eve', 'eve@example.com', 'eve_pwd')
        self.email_notification = EmailNotification.objects.create(
            notification_type='SomeType',
            user=self.user,
            extra=2,
        )

    def test_str(self):
        """
        String representation should be correct.
        """

        self.assertEqual(
            self.email_notification.__str__(), 'SomeType for eve@example.com')

    def test_unicode(self):
        """
        Unicode representation should be correct.
        """

        self.assertEqual(
            self.email_notification.__unicode__(), 'SomeType for eve@example.com')

    def test_repr(self):
        """
        Raw representation should be correct.
        """

        self.assertEqual(
            self.email_notification.__repr__(), 'SomeType for eve@example.com')

    def test_joint_validation(self):
        """
        At least one of user or alt_email must be in the data.
        """

        with self.assertRaises(ValidationError):
            EmailNotification.objects.create(notification_type='event_cc')

    def test_address(self):
        """
        Ensure the address property returns the correct address.

        Expected behavior is that if there is a user, the user.email
        gets returned (even if both fields are present). Else, return the
        alt_email. If neither is filled in, throw an exception.
        """

        self.assertEqual(self.email_notification.address, self.user.email)

        self.email_notification.alt_email = 'fred@dartmouth.edu'
        self.email_notification.save()
        self.email_notification = EmailNotification.objects.get(
            id=self.email_notification.id)
        self.assertEqual(self.email_notification.address, self.user.email)

        email_notification2 = EmailNotification.objects.create(
            notification_type='SomeType',
            alt_email='fred@dartmouth.edu',
            extra=2,
        )
        self.assertEqual(email_notification2.address, 'fred@dartmouth.edu')

        with self.assertRaises(ValidationError):
            email_notification2.alt_email = None
            email_notification2.save()


# class BleachStripTemplateTagTestCase(TestCase):

#     """
#     Test the bleach_strip template tag.
#     """

#     def setUp(self):
#         """
#         Create some default objects for testing.
#         """

#         self.request_factory = RequestFactory()

#     def test_bleach_strip_good_text(self):
#         """
#         Ensure does not alter acceptable text.
#         """

#         str = "This string is fine."
#         clean_str = "This string is fine."

#         output_str = template.Template(
#             "{% load bleach_strip %}"
#             "{% bleach_strip test_str %}"
#         ).render(template.Context({'test_str': str}))

#         self.assertEqual(output_str, clean_str)

#     def test_bleach_strip_bad_text(self):
#         """
#         Ensure does not alter acceptable text.
#         """

#         str = "an <script>evil()</script> example&#39; to &quot;clean"
#         clean_str = "an evil() example' to \"clean"

#         output_str = template.Template(
#             "{% load bleach_strip %}"
#             "{% bleach_strip test_str %}"
#         ).render(template.Context({'test_str': str}))

#         self.assertEqual(output_str, clean_str)


class ImpartMailerTestCase(TestCase):

    """
    Test that the utility mailer works.
    """

    def setUp(self):
        """
        Create the objects needed for testing.
        """

        self.mailer = ImpartMailer()

        os.environ['NORECAPTCHA_TESTING'] = 'True'

        self.factory = RequestFactory()
        self.request = self.factory.get(
            '/crowd_sourcing/research_question_list')

        self.topic_surgery = ResearchQuestionTopicChoiceField.objects.create(
            name="Surgery")
        self.dept_surgery = GeiselDeptChoiceField.objects.create(
            name="Surgery")
        self.default_ip_address = '127.0.0.1'

        self.research_question1 = ResearchQuestion.objects.create(
            first_name="John",
            last_name="Smith",
            email="jsmith@gmail.com",
            institution="Dartmouth College / Geisel",
            department_geisel=self.dept_surgery,
            question="Here is my research question?",
            population="The whole world.",
            key_exposure_of_interest="Human beings.",
            need_lab_data=1,
            need_lab_data_specify="Blood samples",
            topic=self.topic_surgery,
            approved=False,
            ip_address=self.default_ip_address
        )

    def tearDown(self):
        """
        Reset environment.
        """

        os.environ['NORECAPTCHA_TESTING'] = 'False'

    def test_create_body(self):
        """
        Test the utility for generating email bodies.
        """

        question_data = {
            'redirect': 'crowd_sourcing:approve',
            'extra_args': {
                'pk': self.research_question1.id
            }
        }

        approval_url = CryptLink().redir_crypt(question_data)
        signed_url = reverse('signed:signed_url', kwargs={'url': approval_url})

        context_dict = {
            'question': self.research_question1,
            'approval_link': self.request.build_absolute_uri(signed_url)
        }

        mail_template = 'shared/emails/research_question_approval_email.txt'
        expected_body = u"""New Question Submitted: {question}

Field of Inquiry: {topic}

Submitted By: {first_name} {last_name}

Email: {email}

Institution: {institution}

Department: {department_geisel}

Population: {population}

Key Exposure of Interest: {key_exposure_of_interest}

Key Outcome of Interest: {key_outcome_of_interest}

Need Lab/Biometric Data?: Yes

Lab/Biometric Data Needs: {need_lab_data_specify}


---

Please click on the link below to approve this question.

{approval_url}""".format(
            question=self.research_question1.question,
            topic=self.research_question1.topic,
            first_name=self.research_question1.first_name,
            last_name=self.research_question1.last_name,
            email=self.research_question1.email,
            institution=self.research_question1.institution,
            department_geisel=self.research_question1.department_geisel,
            population=self.research_question1.population,
            key_exposure_of_interest=self.research_question1.key_exposure_of_interest,
            key_outcome_of_interest=self.research_question1.key_outcome_of_interest,
            need_lab_data_specify=self.research_question1.need_lab_data_specify,
            approval_url=self.request.build_absolute_uri(signed_url)
            )

        ret_val = self.mailer._create_body(context_dict, mail_template)
        self.assertEqual(ret_val, expected_body)

    def test_send(self):
        """
        Test the send method.
        """

        self.assertEqual(len(mail.outbox), 0)
        self.mailer._send('Subject',
                          'This is a body',
                          ['example@example.com'],
                          ['cc_recipients@example.com'])
        self.assertEqual(len(mail.outbox), 1)
