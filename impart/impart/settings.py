"""Common settings and globals."""

import sys
from os.path import abspath, basename, dirname, join, normpath
from sys import path
from ConfigParser import SafeConfigParser

# PATH CONFIGURATION
# Absolute filesystem path to the Django project directory:
DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the top-level project folder:
SITE_ROOT = dirname(DJANGO_ROOT)

# Site name:
SITE_NAME = basename(DJANGO_ROOT)

# Settings files
SETTINGS_ROOT = dirname(abspath(__file__))
SETTINGS_INI_FILES = normpath(join(SETTINGS_ROOT, 'settings'))

# Add our project to our pythonpath, this way we don't need to type our project
# name in our dotted import paths:
path.append(DJANGO_ROOT)
# END PATH CONFIGURATION

# INI CONFIGURATION
secrets = SafeConfigParser()
secrets.read(normpath(join(SETTINGS_INI_FILES, 'secrets.ini')))

config = SafeConfigParser()
# make sure to set the correct file name in the secrets.ini
config.read(
    normpath(join(SETTINGS_INI_FILES, secrets.get('configfile', 'FILE_NAME'))))
# END INI CONFIGURATION


# SECRET CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = secrets.get('secrets', 'SECRET_KEY')
CSRF_MIDDLEWARE_SECRET = secrets.get('secrets', 'CSRF_MIDDLEWARE_SECRET')

if secrets.has_section('ldap'):
    LDAP_USER = secrets.get('ldap', 'USER')
    LDAP_PASSWORD = secrets.get('ldap', 'PASSWORD')
# END SECRET CONFIGURATION


# DEBUG CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = config.getboolean('debug', 'DEBUG')

DEBUG_TOOLBAR_PATCH_SETTINGS = config.getboolean(
    'debug', 'DEBUG_TOOLBAR_PATCH_SETTINGS')
# END DEBUG CONFIGURATION


# MANAGER CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = tuple(config.items('error mail'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = tuple(config.items('404 mail'))
# END MANAGER CONFIGURATION

# SSL CONFIGURATION
SECURE_PROXY_SSL_HEADER = ('X_FORWARDED_PROTOCOL', 'https')
# SSL_URLS = (

#     '^accounts/',
# )

# SSL_IGNORE_URLS = (
#     '^/i18n_js$',
#     '^/static/',
#     '^/media/',
# )
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
# END SSL PROXY CONFIGURATION

# DATABASE CONFIGURATION
DATABASE_USER = config.get('database', 'DATABASE_USER')
DATABASE_PASSWORD = secrets.get('database', 'DATABASE_PASSWORD')
DATABASE_HOST = config.get('database', 'DATABASE_HOST')
DATABASE_PORT = config.get('database', 'DATABASE_PORT')
DATABASE_ENGINE = config.get('database', 'DATABASE_ENGINE')

# If the DB Engine is SQLite, this sets the file location to the proper
# place in the DB
if DATABASE_ENGINE == 'django.db.backends.sqlite3':
    DATABASE_NAME = normpath(
        join(DJANGO_ROOT, config.get('database', 'DATABASE_NAME')))
    TEST_DATABASE_NAME = normpath(
        join(DJANGO_ROOT, config.get('database', 'TEST_DATABASE_NAME')))
else:
    DATABASE_NAME = config.get('database', 'DATABASE_NAME')
    TEST_DATABASE_NAME = config.get('database', 'TEST_DATABASE_NAME')
# See: https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': DATABASE_ENGINE,
        'NAME': DATABASE_NAME,
        'USER': DATABASE_USER,
        'PASSWORD': DATABASE_PASSWORD,
        'HOST': DATABASE_HOST,
        'PORT': DATABASE_PORT,
    }
}
# END DATABASE CONFIGURATION


# GENERAL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#time-zone
TIME_ZONE = 'America/New_York'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'

# See: https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True

# See: https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
# END GENERAL CONFIGURATION


# MEDIA CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = normpath(join(SITE_ROOT, 'media'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = config.get('assets', 'MEDIA_URL')
# END MEDIA CONFIGURATION


# STATIC FILE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = normpath(join(SITE_ROOT, 'static'))

# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = config.get('assets', 'STATIC_URL')

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = (
    normpath(join(SITE_ROOT, 'impart/static')),
)

# See:
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)
# END STATIC FILE CONFIGURATION


# SITE CONFIGURATION
# Hosts/domain names that are valid for this site
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
# ALLOWED_HOSTS = list(config.get('hosts', 'ALLOWED_HOSTS'))
ALLOWED_HOSTS = [
    'dawson.dartmouth.edu',
    'poindexter.dartmouth.edu',
    'impart.dartmouth.edu',
    'impartdev.dartmouth.edu',
    'localhost',
    '127.0.0.1',
    'imparttest.com',  # Add this to /etc/hosts (used for testing social_auth)
]
# END SITE CONFIGURATION


# FIXTURE CONFIGURATION
# See:
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FIXTURE_DIRS
FIXTURE_DIRS = (
    normpath(join(SITE_ROOT, 'fixtures')),
)
# END FIXTURE CONFIGURATION


# TEMPLATE CONFIGURATION

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            normpath(join(SITE_ROOT, 'impart/templates'))
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'utils.context_processors.web_constants',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'comm_crowd_sourcing.context_processors.app_name',
                'crowd_sourcing.context_processors.app_name',
                'crowd_sourcing_open.context_processors.app_name',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'debug': config.getboolean('debug', 'TEMPLATE_DEBUG'),
        },
    },
]

# END TEMPLATE CONFIGURATION


# MIDDLEWARE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#middleware-classes

DEFAULT_MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'djangosecure.middleware.SecurityMiddleware',
)

if config.has_option('modules', 'ADDON_MIDDLEWARE'):
    ADDON_MIDDLEWARE_CLASSES = (
        tuple([x.strip()
               for x in config.get('modules', 'ADDON_MIDDLEWARE').split(',')])
    )
else:
    ADDON_MIDDLEWARE_CLASSES = ()

MIDDLEWARE_CLASSES = DEFAULT_MIDDLEWARE_CLASSES + ADDON_MIDDLEWARE_CLASSES
# END MIDDLEWARE CONFIGURATION


# URL CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = '%s.urls' % SITE_NAME
# END URL CONFIGURATION


# APP CONFIGURATION
DJANGO_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    'django.contrib.humanize',

    # Admin panel and documentation:
    'django.contrib.admin',
    # 'django.contrib.admindocs',

    # Sitemap
    'django.contrib.sitemaps',
)

# in DJANGO_APPS
THIRD_PARTY_APPS = (
    tuple([x.strip() for x in config.get('modules', 'THIRD_PARTY').split(',')])
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'crowd_sourcing',
    'crowd_sourcing_open',
    'comm_crowd_sourcing',
    'email_utils',
    'utils',
    'signed_link',
    'authentication'
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = THIRD_PARTY_APPS + DJANGO_APPS + LOCAL_APPS

CRISPY_TEMPLATE_PACK = 'bootstrap3'
# END APP CONFIGURATION

# AUTHENTICATION_BACKENDS
# https://docs.djangoproject.com/en/1.6/ref/settings/#std:setting-AUTHENTICATION_BACKENDS
# https://docs.djangoproject.com/en/1.6/topics/auth/customizing/

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'authentication.backends.ImpartAuthBackend.DartBackend',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.google.GoogleOAuth2',
    'social.backends.linkedin.LinkedinOAuth2',
)

# This is necessary so that the default login is rate request list.
# Sorry for the hard code Steven!

LOGIN_REDIRECT_URL = '/'

# LOGGING CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.

# END AUTHENTIATION_BACKENDS

# LOGGING CONFIGURATION

LOG_LEVEL = 'DEBUG'
LOG_SIZE = 1024 * 1024 * 16
LOG_BACKUPS = 30
# LOGGING_CONFIG = None

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'WARNING',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
        'null': {
            'level': LOG_LEVEL,
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'message_file': {
            'level': LOG_LEVEL,
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(SITE_ROOT, 'logs/django.log'),
            'maxBytes': LOG_SIZE,
            'backupCount': LOG_BACKUPS,
            'formatter': 'verbose',
        },
        'catch_file': {
            'level': LOG_LEVEL,
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(SITE_ROOT, 'logs/all.log'),
            'maxBytes': LOG_SIZE,
            'backupCount': LOG_BACKUPS,
            'formatter': 'verbose',
        },
        'wsgi_file': {
            'level': LOG_LEVEL,
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(SITE_ROOT, 'logs/gunicorn.log'),
            'maxBytes': LOG_SIZE,
            'backupCount': LOG_BACKUPS,
            'formatter': 'verbose',
        },
    },
    'formatters': {
        'verbose': {
            'format': '[%(levelname)s] Date: %(asctime)s Module: %(module)s | %(process)d | %(thread)d | %(message)s',
            'datefmt': "%d/%b/%Y %H:%M:%S",
        },
        'simple': {
            'format': '%(levelname)s | %(message)s',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['message_file'],
            'propagate': True,
        },
        'gunicorn': {
            'handlers': ['wsgi_file'],
            'propagate': False,
        },
        '': {
            'handlers': ['catch_file'],
            'propagate': True,
            'level': LOG_LEVEL,
        },


    }
}


# logging.config.dictConfig(LOGGING)

# END LOGGING CONFIGURATION

# WSGI CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'impart.wsgi.application'
# END WSGI CONFIGURATION

# EMAIL CONFIGURATION

if config.has_section('email'):
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host
    EMAIL_HOST = config.get('email', 'EMAIL_HOST')

    # See:
    # https://docs.djangoproject.com/en/dev/ref/settings/#email-host-password
    EMAIL_HOST_PASSWORD = secrets.get('email', 'EMAIL_HOST_PASSWORD')

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-host-user
    EMAIL_HOST_USER = config.get('email', 'EMAIL_HOST')

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-port
    EMAIL_PORT = config.get('email', 'EMAIL_PORT')

    # See:
    # https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
    EMAIL_SUBJECT_PREFIX = '[%s] ' % SITE_NAME

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-use-tls
    EMAIL_USE_TLS = config.getboolean('email', 'EMAIL_USE_TLS')

    # See: https://docs.djangoproject.com/en/dev/ref/settings/#server-email
    SERVER_EMAIL = config.get('email', 'SERVER_EMAIL')
else:
    # See: https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
# END EMAIL CONFIGURATION

# CACHE CONFIGURATION
# See: https://docs.djangoproject.com/en/dev/ref/settings/#caches
# CACHES = {}
# CACHES = {
#    'default': {
#        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#    }
# }
# END CACHE CONFIGURATION

# if len(sys.argv) > 1 and sys.argv[1] == 'test':
#     logging.disable(logging.CRITICAL)

# Disable captcha for tests for simple-captcha
if 'test' in sys.argv:
    CAPTCHA_TEST_MODE = True
CAPTCHA_FLITE_PATH = '/usr/bin/flite'
# Recaptcha information
NORECAPTCHA_SITE_KEY = '6Let1fwSAAAAAHCFy87VD2LfVg_S5Q2ss4f2shML '
NORECAPTCHA_SECRET_KEY = secrets.get('recaptcha', 'PRIVATE_KEY')
NORECAPTCHA_USE_SSL = True

# Set to False to disable rate-limiting across the board.
RATELIMIT_ENABLE = True

# django-secure module configuration
SECURE_HSTS_SECONDS = 60 * 60 * 24 * 365
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_FRAME_DENY = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_SSL_REDIRECT = False
SECURE_SSL_HOST = None

# Facebook keys for authentication
SOCIAL_AUTH_FACEBOOK_KEY = secrets.get('facebook', 'SOCIAL_AUTH_FACEBOOK_KEY')
SOCIAL_AUTH_FACEBOOK_SECRET = secrets.get(
    'facebook', 'SOCIAL_AUTH_FACEBOOK_SECRET')
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

# LinkedIn keys for authentication
SOCIAL_AUTH_LINKEDIN_OAUTH2_KEY = secrets.get(
    'linkedin', 'SOCIAL_AUTH_LINKEDIN_KEY')
SOCIAL_AUTH_LINKEDIN_OAUTH2_SECRET = secrets.get(
    'linkedin', 'SOCIAL_AUTH_LINKEDIN_SECRET')
SOCIAL_AUTH_LINKEDIN_OAUTH2_SCOPE = ['r_emailaddress']
SOCIAL_AUTH_LINKEDIN_OAUTH2_FIELD_SELECTORS = ['email-address']
SOCIAL_AUTH_LINKEDIN_OAUTH2_EXTRA_DATA = [('emailAddress', 'email_address')]

# Google keys for authentication
SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = secrets.get(
    'google', 'SOCIAL_AUTH_GOOGLE_OAUTH2_KEY')
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = secrets.get(
    'google', 'SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET')
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = [
    'https://www.googleapis.com/auth/userinfo.email']

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    # 'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'utils.utils.save_email',
)

# Suppress false positive test warning introduced in 1.7 and being
# removed in 1.8
TEST_RUNNER = 'django.test.runner.DiscoverRunner'
